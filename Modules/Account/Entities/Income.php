<?php

namespace Modules\Account\Entities;

use App\Company;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Modules\Livestock\Entities\LiveStock;

class Income extends Model
{
    protected $fillable = [];
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
