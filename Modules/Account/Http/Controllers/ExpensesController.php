<?php

namespace Modules\Account\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Modules\Account\Entities\Expenses;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        // return view('account::index');
        $expenses = Expenses::get();
        if ($expenses->count() < 1 ) {
            $message = 'Not found';
            return response()->json(compact('message'), 404);
        }
        return response()->json(compact('categories'), 200);

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('account::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $expenses = new Expenses();
        $expenses->description = $request->description;
        $expenses->company_id = $request->company_id;
        $expenses->livestock_id = $request->livestock_id;
        $expenses->amount = $request->amount;
        $expenses->approved_by = $request->approved_by;
        $expenses->confirmed_by = $request->confirmed_by;
        $expenses->save();
        if ($expenses->save()) {
            $message = 'was saved successful';
            return response()->json(compact('message'), 200);
        }
        $message = 'couldnt be successful';
        return response()->json(compact('message'), 500);

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('account::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('account::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $expenses)
    {
        $expenses = Expenses::find($expenses);
        $expenses->description = $request->description;
        $expenses->company_id = $request->company_id;
        $expenses->livestock_id = $request->livestock_id;
        $expenses->amount = $request->amount;
        $expenses->approved_by = $request->approved_by;
        $expenses->confirmed_by = $request->confirmed_by;

        if ($expenses->save()) {
            $message = 'Action was updated';
            return response()->json(compact('message'), 200);
        }
        $message = 'couldnt be updated to';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($expenses)
    {
        $expenses = Expenses::find($expenses);
        $expenses->delete();

        $message = 'was deleted';
        return response()->json(compact('message'), 500);
    }
}
