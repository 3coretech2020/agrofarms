<?php

namespace Modules\Account\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Account\Entities\Income;
use Modules\Sales\Entities\EggSale;
use Modules\Sales\Entities\LiveStockSale;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $date_from = Carbon::now()->startOfMonth();
        $date_to = Carbon::now()->endOfMonth();
        $panel = 'month';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        $egg_sales = EggSale::where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->orderBy('id', 'DESC')->get();
        foreach ($egg_sales as $egg_sale) {
            $egg_sale->description = 'Sale of ' . $egg_sale->quantity_in_crates . ' crate(s) of eggs';
            $egg_sale->amount = $egg_sale->total_amount;
        }
        $livestock_sales = LiveStockSale::where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->orderBy('id', 'DESC')->get();
        foreach ($livestock_sales as $livestock_sale) {
            $livestock_sale->description = 'Sale of ' . $livestock_sale->quantity . ' ' . $livestock_sale->liveStock->name;
            $livestock_sale->amount = $livestock_sale->amount;
        }
        $incomes = $egg_sales->mergeRecursive($livestock_sales);
        return response()->json(compact('incomes'), 200);



        // return view('account::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('account::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $income = new Income();
        $income->company_id = $request->company_id;
        $income->description = $request->description;
        $income->amount = $request->amount;
        $income->livestock_id = $request->livestock_id;
        $income->comfirmed_by = $request->comfirmed_by;
        $income->customer = $request->customer;
        $income->save();

        if ($income->save()) {
            $message = 'was updated';
            return response()->json(compact('message'), 200);
        }
        $message = 'couldnt be updated to';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('account::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('account::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $income = Income::find($id);
        $income->company_id = $request->company_id;
        $income->description = $request->description;
        $income->amount = $request->amount;
        $income->livestock_id = $request->livestock_id;
        $income->comfirmed_by = $request->comfirmed_by;
        $income->customer = $request->customer;
        $income->save();

        if ($income->save()) {
            $message = 'was updated';
            return response()->json(compact('message'), 200);
        }
        $message = 'couldnt be updated to';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $income = Income::find($id);
        $income->delete();

        $message = 'was deleted';
        return response()->json(compact('message'), 500);
    }
}
