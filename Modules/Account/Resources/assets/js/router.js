/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const accountRoutes = {
  path: '/accounts',
  component: Layout,
  redirect: '/accounts/expenses',
  name: 'Account',
  alwaysShow: true,
  meta: {
    title: 'Account',
    icon: 'el-icon-money',
    roles: ['admin', 'auditor'],
  },
  children: [
    // {
    //   component: () => import('./views/income/index.vue'),
    //   path: '/accounts/income',
    //   name: 'Income',
    //   meta: {
    //     title: 'Income',
    //     roles: ['admin', 'auditor'],
    //     permissions: ['manage income'],
    //   },
    // },
    {
      component: () => import ('./views/expenses/index.vue'),
      path: '/accounts/expenses',
      name: 'Expenses',
      meta: {
        title: 'Expenses',
        roles: ['admin', 'auditor'],
        permissions: ['manage expenses'],
      },
    },
  ],
};
export default accountRoutes;
