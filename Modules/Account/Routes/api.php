<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/account', function (Request $request) {
//     return $request->user();
// });
Route::prefix('account')->group(function () {
    Route::prefix('expenses')->group(function () {
        Route::get('/', 'ExpensesController@index');
        Route::post('/store', 'ExpensesController@store');
        Route::put('/update/{account}', 'ExpensesController@update');
        Route::delete('/destroy/{account}', 'ExpensesController@destroy');
    });
    // Income Route
    Route::prefix('income')->group(function () {
        Route::get('/', 'IncomeController@index');
        Route::post('/store', 'IncomeController@store');
        Route::put('/update/{account}', 'IncomeController@update');
        Route::delete('/destroy/{account}', 'IncomeController@destroy');
    });
});
