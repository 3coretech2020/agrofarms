<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('crops', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('company_id');
            $table->timestamps();
        });
        Schema::create('cultivated_crops', function (Blueprint $table) {
            $table->id();
            $table->integer('crop_id');
            $table->string('batch_no');
            $table->integer('quantity_of_seedlings');
            $table->string('unit_of_measurement')->nullable();
            $table->timestamps();
        });
        Schema::create('harvested_crops', function (Blueprint $table) {
            $table->id();
            $table->integer('crop_id');
            $table->integer('cultivated_crop_id');
            $table->integer('batch_no');
            $table->integer('quantity');
            $table->string('unit_of_measurement')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('crops');
        Schema::dropIfExists('cultivated_crops');
        Schema::dropIfExists('harvested_crops');
    }
}
