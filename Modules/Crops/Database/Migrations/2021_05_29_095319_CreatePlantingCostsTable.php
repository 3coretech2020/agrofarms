<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantingCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planting_costs', function (Blueprint $table) {
            $table->id();
            $table->integer('crop_id');
            $table->string('batch_no')->nullable();
            $table->double('total_amount', 10, 2)->default(0);
            $table->timestamps();
        });
        Schema::create('planting_cost_details', function (Blueprint $table) {
            $table->id();
            $table->integer('planting_cost_id');
            $table->integer('quantity');
            $table->double('rate', 10, 2);
            $table->double('amount', 10, 2);
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planting_costs');
        Schema::dropIfExists('planting_cost_details');
    }
}
