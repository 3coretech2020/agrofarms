<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoldCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_crops', function (Blueprint $table) {
            $table->id();
            $table->integer('crop_id');
            $table->integer('harvested_crop_id');
            $table->integer('quantity');
            $table->double('rate', 10, 2);
            $table->double('amount', 10, 2);
            $table->string('description');
            $table->integer('recorded_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_crops');
    }
}
