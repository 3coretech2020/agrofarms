<?php

namespace Modules\Crops\Entities;

use App\Company;
use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function cultivated()
    {
        return $this->hasMany(CultivatedCrop::class, 'crop_id', 'id');
    }
    public function harvested()
    {
        return $this->hasMany(HarvestedCrop::class, 'crop_id', 'id');
    }
    public function costs()
    {
        return $this->hasMany(PlantingCost::class, 'crop_id', 'id');
    }
    public function soldCrops()
    {
        return $this->hasMany(SoldCrop::class, 'crop_id', 'id');
    }
}
