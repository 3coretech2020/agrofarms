<?php

namespace Modules\Crops\Entities;

use Illuminate\Database\Eloquent\Model;

class CultivatedCrop extends Model
{
    public function crop()
    {
        return $this->belongsTo(Crop::class, 'crop_id', 'id');
    }
    public function harvested()
    {
        return $this->hasMany(HarvestedCrop::class, 'cultivated_crop_id', 'id');
    }
}
