<?php

namespace Modules\Crops\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HarvestedCrop extends Model
{
    public function crop()
    {
        return $this->belongsTo(Crop::class, 'crop_id', 'id');
    }
    public function cultivated()
    {
        return $this->belongsTo(CultivatedCrop::class, 'cultivated_crop_id', 'id');
    }
    public function soldCrops()
    {
        return $this->hasMany(SoldCrop::class, 'crop_id', 'id');
    }
}
