<?php

namespace Modules\Crops\Entities;

use Illuminate\Database\Eloquent\Model;

class PlantingCost extends Model
{
    public function crop()
    {
        return $this->belongsTo(Crop::class, 'crop_id', 'id');
    }
    public function details()
    {
        return $this->hasMany(PlantingCostDetail::class, 'planting_cost_id', 'id');
    }
}
