<?php

namespace Modules\Crops\Entities;

use Illuminate\Database\Eloquent\Model;

class PlantingCostDetail extends Model
{
    public function plantCost()
    {
        return $this->belongsTo(PlantingCost::class, 'planting_cost_id', 'id');
    }
}
