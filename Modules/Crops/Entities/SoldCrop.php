<?php

namespace Modules\Crops\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SoldCrop extends Model
{
    public function crop()
    {
        return $this->belongsTo(Crop::class, 'crop_id', 'id');
    }
    public function harvest()
    {
        return $this->belongsTo(HarvestedCrop::class, 'harvested_crop_id', 'id');
    }
    public function recorder()
    {
        return $this->belongsTo(User::class, 'recorded_by', 'id');
    }
}
