<?php

namespace Modules\Crops\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Crops\Entities\Crop;
use Modules\Crops\Entities\HarvestedCrop;
use Modules\Crops\Entities\SoldCrop;

class CropsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $crops = Crop::orderBy('name')->get();
        return response()->json(compact('crops'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $crop = Crop::where('name', $name)->first();
        if (!$crop) {
            $crop = new Crop();
            $crop->name = $name;
            $crop->save();
        }
        $this->fillUnsyncTable('crops', $crop->id);
        return $this->show($crop);
    }

    public function show(Crop $crop)
    {
        $crop =  $crop->with(['cultivated', 'harvested.cultivated', 'costs.details', 'soldCrops.recorder'])->find($crop->id);
        // return response()->json(compact('crop'), 200);
        $expenses =  $crop->costs()->groupBy('crop_id')->select(\DB::raw('SUM(total_amount) as expenditure'))->first();
        $income =  $crop->soldCrops()->groupBy('crop_id')->select(\DB::raw('SUM(amount) as income'))->first();
        return response()->json(compact('crop', 'expenses', 'income'), 200);
    }
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function sales(Request $request)
    {
        $user = $this->getCurrentUser();
        $company_id = $this->getUserCompany();
        $sold_crop = new SoldCrop();
        $sold_crop->crop_id = $request->crop_id;
        $sold_crop->company_id = $company_id;
        $sold_crop->quantity = $request->quantity;
        $sold_crop->rate = $request->rate;
        $sold_crop->amount = $request->amount;
        $sold_crop->description = $request->description;
        $sold_crop->recorded_by = $user->id;
        if ($sold_crop->save()) {
            $this->fillUnsyncTable('sold_crops', $sold_crop->id);
            $this->sellHarvestedCrops($sold_crop->quantity);
        }
        return response()->json([], 200);
    }

    private function sellHarvestedCrops($quantity_for_sale)
    {
        $harvested_crops = HarvestedCrop::whereRaw('quantity - sold > 0')->orderBy('id')->get();
        foreach ($harvested_crops as $harvested_crop) {
            $quantity_available = $harvested_crop->quantity - $harvested_crop->sold;
            if ($quantity_for_sale > $quantity_available) {
                // what to be sold is greater than what is available
                $harvested_crop->sold += $quantity_available;
                $harvested_crop->save();

                $quantity_for_sale -= $quantity_available;
                $this->fillUnsyncTable('harvested_crops', $harvested_crop->id);
            } else {
                // what to be sold is less than or equal to what is available
                $harvested_crop->sold += $quantity_for_sale;
                $harvested_crop->save();
                $quantity_for_sale = 0;
                $this->fillUnsyncTable('harvested_crops', $harvested_crop->id);
                break;
            }
        }
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Crop $crop)
    {
        //
    }
}
