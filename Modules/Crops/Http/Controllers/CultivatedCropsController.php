<?php

namespace Modules\Crops\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Crops\Entities\CultivatedCrop;

class CultivatedCropsController extends Controller
{
    ///////////////////////////CULTIVATED CROPS ////////////////////////////
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function index(Request $request)
    {
        $cultivated_crops = CultivatedCrop::where('crop_id', $request->crop_id)->orderBy('id', 'DESC')->get();
        return response()->json(compact('cultivated_crops'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser();
        $cultivated_crop = new CultivatedCrop();
        $cultivated_crop->crop_id = $request->crop_id;
        $cultivated_crop->quantity_of_seedlings = $request->quantity_of_seedlings;
        $cultivated_crop->unit_of_measurement = $request->unit_of_measurement;
        $cultivated_crop->date = date('Y-m-d', strtotime($request->date));
        if ($cultivated_crop->save()) {

            $cultivated_crop->batch_no = $this->getBatchNo('PLT', $cultivated_crop->id);
            $cultivated_crop->save();

            $this->fillUnsyncTable('cultivated_crops', $cultivated_crop->id);

            // log this activity
            $title = 'Planting of ' . $cultivated_crop->crop->name . ' added';
            $description = 'Planting of ' . $cultivated_crop->quantity_of_seedlings . ' ' . $cultivated_crop->unit_of_measurement . ' of ' . $cultivated_crop->crop->name . " with batch number: ($cultivated_crop->batch_no) was added by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($cultivated_crop);
        }
        return response()->json(['message' => 'An error occured'], 500);
    }

    public function show(CultivatedCrop $cultivated_crop)
    {
        $cultivated_crop =  $cultivated_crop->with(['crop', 'harvested'])->find($cultivated_crop->id);
        return response()->json(compact('cultivated_crop'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('crops::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
