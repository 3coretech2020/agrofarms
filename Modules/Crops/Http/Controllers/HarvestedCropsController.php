<?php

namespace Modules\Crops\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Crops\Entities\HarvestedCrop;
use Modules\Crops\Entities\SoldCrop;

class HarvestedCropsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function index(Request $request)
    {
        $harvested_crops = HarvestedCrop::where('crop_id', $request->crop_id)->orderBy('id', 'DESC')->get();
        return response()->json(compact('harvested_crops'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser();
        $harvested_crop = new HarvestedCrop();
        $harvested_crop->crop_id = $request->crop_id;
        $harvested_crop->cultivated_crop_id = $request->cultivated_crop_id;
        $harvested_crop->quantity = $request->quantity;
        $harvested_crop->unit_of_measurement = $request->unit_of_measurement;
        $harvested_crop->date = date('Y-m-d', strtotime($request->date));
        if ($harvested_crop->save()) {

            $harvested_crop->batch_no = $this->getBatchNo('HAV', $harvested_crop->id);
            $harvested_crop->save();

            $this->fillUnsyncTable('harvested_crops', $harvested_crop->id);
            // log this activity
            $title = 'Harvest of ' . $harvested_crop->crop->name . ' added';
            $description = 'Harvest of ' . $harvested_crop->quantity . ' ' . $harvested_crop->unit_of_measurement . ' of ' . $harvested_crop->crop->name . " with batch number: ($harvested_crop->batch_no) was added by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($harvested_crop);
        }
        return response()->json(['message' => 'An error occured'], 500);
    }

    public function show(HarvestedCrop $harvested_crop)
    {
        $harvested_crop =  $harvested_crop->with(['crop', 'cultivated'])->find($harvested_crop->id);
        return response()->json(compact('harvested_crop'), 200);
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
