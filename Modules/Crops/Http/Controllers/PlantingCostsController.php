<?php

namespace Modules\Crops\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Crops\Entities\PlantingCost;
use Modules\Crops\Entities\PlantingCostDetail;

class PlantingCostsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $planting_costs = PlantingCost::with(['crop', 'details'])->orderBy('id', 'DESC')->get();
        return response()->json(compact('planting_costs'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser();
        $details = json_decode(json_encode($request->details));
        $planting_cost = new PlantingCost();
        $planting_cost->crop_id = $request->crop_id;
        $planting_cost->total_amount = $request->amount;
        if ($planting_cost->save()) {
            $planting_cost->batch_no = $this->getBatchNo('PCT', $planting_cost->id);
            $planting_cost->save();
            $this->fillUnsyncTable('planting_costs', $planting_cost->id);
            $this->createCostDetails($planting_cost, $details);

            // $title = 'Cost of Planting of ' . $planting_cost->crop->name . ' added';
            // $description = 'Planting of ' . $cultivated_crop->quantity_of_seedlings . ' ' . $cultivated_crop->unit_of_measurement . ' of ' . $cultivated_crop->crop->name . " with batch number: ($cultivated_crop->batch_no) was added by " . $user->name;
            // $roles = ['auditor', 'farm-officer'];
            // $this->logUserActivity($title, $description, $roles);
            return $this->show($planting_cost);
        }
        // return $this->show($planting_cost);
    }

    private function createCostDetails($planting_cost, $items)
    {
        foreach ($items as $item) {
            // $batches = $item->batches;
            $details = new PlantingCostDetail();
            $details->planting_cost_id = $planting_cost->id;
            $details->quantity = $item->quantity;
            $details->rate = $item->rate;
            $details->amount = $item->amount;
            $details->description = $item->description;

            $details->save();
            $this->fillUnsyncTable('planting_cost_details', $details->id);
        }
    }
    public function show(PlantingCost $planting_cost)
    {
        $planting_cost =  $planting_cost->with(['crop', 'details'])->find($planting_cost->id);
        return response()->json(compact('planting_cost'), 200);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
