/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const cropsRoutes = {
  path: '/crops',
  component: Layout,
  redirect: '/',
  name: 'Crops',
  alwaysShow: true,
  meta: {
    title: 'Crops',
    icon: 'el-icon-apple',
    // roles: ['admin', 'auditor'],
  },
  children: [
    {
      component: () => import('./views'),
      path: '/crops/',
      name: 'ManageCrops',
      meta: {
        title: 'Manage Crops',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage income'],
      },
    },
    {
      path: '/crops/details/:id(\\d+)',
      component: () => import('./views/CropDetails'),
      name: 'CropDetails',
      meta: {
        title: 'Details', noCache: true,
        // permissions: ['manage user'],
      },
      hidden: true,
    },
  ],
};
export default cropsRoutes;
