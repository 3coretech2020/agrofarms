<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('crops')->group(function () {
        //////////////////////livestock Routes///////////////

        Route::get('/', 'CropsController@index');
        Route::get('/details/{crop}', 'CropsController@show');
        Route::post('/store', 'CropsController@store');
        Route::post('/sales', 'CropsController@sales');
        Route::put('/update/{crop}', 'CropsController@update');
        Route::delete('/destroy/{crop}', 'CropsController@destroy');

        ////////////////Cultivated Crops///////////////////////////
        Route::prefix('cultivated')->group(function () {
            Route::get('/', 'CultivatedCropsController@index');
            Route::post('/store', 'CultivatedCropsController@store');
            Route::put('/update/{cultivated_crop}', 'CultivatedCropsController@update');
            Route::delete('/destroy/{cultivated_crop}', 'CultivatedCropsController@destroy');
            Route::put('/confirm-batch/{batch}', 'CultivatedCropsController@confirmBatch');
            Route::get('/details/{cultivated_crop}', 'CultivatedCropsController@show');
        });
        ////////////////Harvested Crops///////////////////////////
        Route::prefix('harvested')->group(function () {
            Route::get('/', 'HarvestedCropsController@index');
            Route::post('/store', 'HarvestedCropsController@store');
            Route::put('/update/{harvested_crop}', 'HarvestedCropsController@update');
            Route::delete('/destroy/{harvested_crop}', 'HarvestedCropsController@destroy');
            Route::put('/confirm-batch/{batch}', 'HarvestedCropsController@confirmBatch');
            Route::get('/details/{harvested_crop}', 'HarvestedCropsController@show');
        });
        Route::prefix('cost')->group(function () {
            Route::get('/', 'PlantingCostsController@index');
            Route::post('/store', 'PlantingCostsController@store');
            Route::put('/update/{harvested_crop}', 'PlantingCostsController@update');
            Route::delete('/destroy/{harvested_crop}', 'PlantingCostsController@destroy');
            Route::put('/confirm-batch/{batch}', 'PlantingCostsController@confirmBatch');
            Route::get('/details/{harvested_crop}', 'PlantingCostsController@show');
        });
    });
});
