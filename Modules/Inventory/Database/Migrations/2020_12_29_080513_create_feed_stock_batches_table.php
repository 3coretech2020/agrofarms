<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedStockBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_stock_batches', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_id');
            $table->integer('feed_id');
            $table->string('batch_no')->nullable();;
            $table->double('amount');
            $table->integer('quantity_stocked');
            $table->integer('consumed')->default(0);
            $table->integer('balance');
            $table->double('rate', 10, 2);
            $table->string('net_weight');
            $table->string('package_type');
            $table->string('unit_of_measurement');
            $table->integer('stocked_by');
            $table->integer('confirmed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_stock_batches');
    }
}
