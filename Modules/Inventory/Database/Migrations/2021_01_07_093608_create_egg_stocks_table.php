<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEggStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('egg_stocks', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_id');
            $table->integer('livestock_batch_id');
            $table->string('batch_no');
            $table->integer('quantity');
            $table->integer('sold');
            $table->integer('balance');
            $table->integer('stocked_by');
            $table->integer('confirmed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('egg_stocks');
    }
}
