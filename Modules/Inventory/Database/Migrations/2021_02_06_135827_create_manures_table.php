<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manures', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('quantity_in_stock');
            $table->integer('sold');
            $table->integer('balance');
            $table->string('package_type');
            $table->integer('last_stocker');
            $table->integer('last_quantity_stocked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manures');
    }
}
