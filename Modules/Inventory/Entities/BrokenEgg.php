<?php

namespace Modules\Inventory\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BrokenEgg extends Model
{
    public function savedBy()
    {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
