<?php

namespace Modules\Inventory\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;

class EggStock extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function livestockBatch()
    {
        return $this->belongsTo(LiveStockBatch::class, 'livestock_batch_id', 'id');
    }
    public function stockOfficer()
    {
        return $this->belongsTo(User::class, 'stocked_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
