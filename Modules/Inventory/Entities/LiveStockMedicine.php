<?php

namespace Modules\Inventory\Entities;

use App\Company;
use Illuminate\Database\Eloquent\Model;
use Modules\Livestock\Entities\LiveStock;

class LiveStockMedicine extends Model
{


    protected $fillable = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
}
