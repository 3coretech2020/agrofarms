<?php

namespace Modules\Inventory\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Manure extends Model
{


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function lastStocker()
    {
        return $this->belongsTo(User::class, 'last_stocker', 'id');
    }
}
