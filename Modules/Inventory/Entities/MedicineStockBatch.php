<?php

namespace Modules\Inventory\Entities;

use App\Company;
use Illuminate\Database\Eloquent\Model;
use Modules\Livestock\Entities\LiveStock;
use App\User;

class MedicineStockBatch extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function medicine()
    {
        return $this->belongsTo(LiveStockMedicine::class, 'medicine_id', 'id');
    }
    public function stockOfficer()
    {
        return $this->belongsTo(User::class, 'stocked_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
