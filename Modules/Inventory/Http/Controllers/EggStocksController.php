<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\BrokenEgg;
use Modules\Inventory\Entities\LiveStockEgg;
use Modules\Inventory\Entities\EggStock;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;

class EggStocksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $eggs_in_stock = EggStock::with(['liveStock.category', 'livestockBatch', 'stockOfficer', 'confirmer'])->where('company_id', $company_id)/* ->where('balance', '>', 0)*/->orderBy('id', 'DESC')->get();
        // $total_eggs = EggStock::groupBy('company_id')->select('*', \DB::raw('SUM(quantity) as quantity'), \DB::raw('SUM(sold) as sold'), \DB::raw('SUM(broken) as broken'), \DB::raw('SUM(balance) as balance'))->where('company_id', $company_id)->first();
        return response()->json(compact('eggs_in_stock'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function fetchLivestockBatches($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $livestock_batches = LiveStockBatch::where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->whereRaw('quantity_stocked - quantity_dead - quantity_sold > 0')->where('confirmed_by', '!=', null)->orderBy('batch_no')->get();
        return response()->json(compact('livestock_batches'), 200);
    }

    public function fetchEggStocks($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $egg_stocks = EggStock::where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->where('balance', '>', 0)->where('confirmed_by', '!=', null)->orderBy('batch_no')->get();
        return response()->json(compact('egg_stocks'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $prefix = 'EGG-';
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $egg_stock = new EggStock();
        $egg_stock->company_id = $company_id;;
        $egg_stock->livestock_id = $request->livestock_id;
        $egg_stock->livestock_batch_id = $request->livestock_batch_id;
        $egg_stock->quantity = $request->quantity;
        $egg_stock->balance = $request->quantity;
        $egg_stock->stocked_by = $user->id;

        if ($egg_stock->save()) {

            $egg_stock->batch_no = $this->getBatchNo($prefix, $egg_stock->id);
            $egg_stock->save();

            // log this activity
            $title = 'New of Eggs for ' . $egg_stock->livestockBatch->batch_no . ' added';
            $description = $egg_stock->quantity . ' ' . $egg_stock->liveStock->name . " eggs with batch no.: ($egg_stock->batch_no) was added to stock by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($egg_stock);
        }
        return $this->index();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(EggStock $egg_stock)
    {
        $egg_stock =  $egg_stock->with(['liveStock.category', 'livestockBatch', 'stockOfficer', 'confirmer'])->find($egg_stock->id);
        return response()->json(compact('egg_stock'), 200);
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, EggStock $egg_stock)
    {
        //
        $user = $this->getCurrentUser();
        if ($egg_stock->confirmed_by === null) {
            # code...

            $egg_stock->livestock_id = $request->livestock_id;
            $egg_stock->livestock_batch_id = $request->livestock_batch_id;
            $egg_stock->quantity = $request->quantity;
            $egg_stock->balance = $request->quantity - $egg_stock->sold - $egg_stock->broken;
            if ($egg_stock->save()) {
                // log this activity
                $title = 'Details of eggs in stock for ' . $egg_stock->batch_no . ' modified';
                $description = $egg_stock->liveStock->name . "'s egg details with batch no.: ($egg_stock->batch_no) was updated by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($egg_stock);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmStock(Request $request, EggStock $egg_stock)
    {
        //
        $user = $this->getCurrentUser();
        $egg_stock->confirmed_by = $user->id;
        $egg_stock->save();

        $this->fillUnsyncTable('egg_stocks', $egg_stock->id);
        // log this activity
        $title = 'Eggs stock information confirmed';
        $description = $egg_stock->quantity . ' ' . $egg_stock->liveStock->name . " eggs with batch number: ($egg_stock->batch_no) was confirmed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->show($egg_stock);
    }
    public function allBrokenEggs()
    {
        $company_id = $this->getUserCompany();
        $broken_eggs = BrokenEgg::with(['savedBy', 'confirmer'])->where('company_id', $company_id)->orderBy('id', 'DESC')->get();
        return response()->json(compact('broken_eggs'), 200);
    }
    public function saveBrokenEggs(Request $request)
    {
        //
        $user = $this->getCurrentUser();
        $company_id = $this->getUserCompany();
        $broken_egg = new BrokenEgg();
        $broken_egg->company_id = $company_id;;
        $broken_egg->quantity = $request->quantity;
        $broken_egg->date = $request->date;
        $broken_egg->saved_by = $user->id;

        if ($broken_egg->save()) {

            // log this activity
            $title = 'Broken Eggs recorded';
            $description = $broken_egg->quantity  . " broken eggs was recorded by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->showBrokenEgg($broken_egg);
        }
    }
    public function showBrokenEgg(BrokenEgg $broken_egg)
    {
        $broken_egg =  $broken_egg->with(['savedBy', 'confirmer'])->find($broken_egg->id);
        return response()->json(compact('broken_egg'), 200);
    }
    public function confirmBrokenEggs(Request $request, BrokenEgg $broken_egg)
    {
        //
        $user = $this->getCurrentUser();
        $broken_egg->confirmed_by = $user->id;
        if ($broken_egg->save()) {
            $this->fillUnsyncTable('broken_eggs', $broken_egg->id);
            // effect the change
            $this->deductStockedEggs($broken_egg->quantity);
        }

        // log this activity
        $title = 'Broken Eggs record confirmed';
        $description = $broken_egg->quantity . ' broken eggs record was confirmed by ' . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->showBrokenEgg($broken_egg);
    }
    private function deductStockedEggs($quantity)
    {
        $company_id = $this->getUserCompany();
        $egg_stocks = EggStock::where('company_id', $company_id)->where('balance', '>', 0)->get();
        foreach ($egg_stocks as $egg_stock) {
            $balance = $egg_stock->balance;
            if ($balance >= $quantity) {
                $egg_stock->balance -= $quantity;
                $egg_stock->broken += $quantity;
                $egg_stock->save();
                $quantity = 0;
                $this->fillUnsyncTable('egg_stocks', $egg_stock->id);
                break;
            } else {
                $egg_stock->balance = 0;
                $egg_stock->broken += $balance;
                $egg_stock->save();
                $quantity -= $balance;
                $this->fillUnsyncTable('egg_stocks', $egg_stock->id);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function updateBrokenEggs(Request $request, BrokenEgg $broken_egg)
    {
        //
        $user = $this->getCurrentUser();
        if ($broken_egg->confirmed_by == null) {
            $old_quantity = $broken_egg->quantity;
            $broken_egg->quantity = $request->quantity;
            $broken_egg->date = $request->date;

            if ($broken_egg->save()) {
                $title = 'Broken eggs record modified';
                $description = $old_quantity . ' broken eggs was changed to ' . $broken_egg->quantity . ' by ' . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->showBrokenEgg($broken_egg);
            }
        }
        $message = 'Broken eggs record could not be updated';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroyBrokenEggs(BrokenEgg $broken_egg)
    {
        $user = $this->getCurrentUser();
        if ($broken_egg->confirmed_by == null) {


            $title = 'Broken eggs record deleted';
            $description = $broken_egg->quantity . ' broken eggs record for was deleted by ' . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            $broken_egg->delete();
            return response()->json([], 204);
        }
        $message = 'Broken eggs record could not be deleted';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(EggStock $egg_stock)
    {
        // $user = $this->getCurrentUser();
        // $title = 'Details of eggs in stock for ' . $egg_stock->egg->name . ' deleted';
        // $description = $egg_stock->egg->name . "'s egg details with batch no.: ($egg_stock->batch_no) was deleted by " . $user->name;
        // $roles = ['auditor', 'farm-officer'];
        // $this->logUserActivity($title, $description, $roles);

        // $egg_stock->delete();
        // return response()->json([], 204);
    }
}
