<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\FeedStockBatch;
use Modules\Inventory\Entities\LiveStockFeed;
use Modules\Livestock\Entities\LiveStock;

class FeedStocksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $request = request()->all();
        $company_id = $this->getUserCompany();
        $livestock_id = $request['livestock_id'];
        $feeds_in_stock = FeedStockBatch::with(['liveStock.category', 'feed', 'stockOfficer', 'confirmer'])->where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->orderBy('batch_no', 'DESC')->get();
        return response()->json(compact('feeds_in_stock'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function fetchLivestockFeeds($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $feeds = LiveStockFeed::where('livestock_id', $livestock_id)->get();
        return response()->json(compact('feeds'), 200);
    }

    public function fetchBatches($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $feed_stock_batches = FeedStockBatch::groupBy('feed_id')->with('feed')->where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->where('balance', '>', 0)->where('confirmed_by', '!=', null)->select('*', \DB::raw('SUM(balance) as balance'))->orderBy('batch_no')->get();
        return response()->json(compact('feed_stock_batches'), 200);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $prefix = 'FD-';
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $feed_stock_batch = new FeedStockBatch();
        $feed_stock_batch->company_id = $company_id;;
        $feed_stock_batch->livestock_id = $request->livestock_id;
        $feed_stock_batch->feed_id = $request->feed_id;
        $feed_stock_batch->quantity_stocked = $request->quantity_stocked;
        $feed_stock_batch->balance = $request->quantity_stocked;
        $feed_stock_batch->stocked_by = $user->id;
        $feed_stock_batch->rate = $request->rate;
        $feed_stock_batch->amount = $request->rate * $request->quantity_stocked;
        $feed_stock_batch->net_weight = $request->net_weight;
        $feed_stock_batch->package_type = $request->package_type;
        $feed_stock_batch->unit_of_measurement = $request->unit_of_measurement;

        if ($feed_stock_batch->save()) {

            $feed_stock_batch->batch_no = $this->getBatchNo($prefix, $feed_stock_batch->id);
            $feed_stock_batch->save();

            // log this activity
            $title = 'New Batch of Feeds for ' . $feed_stock_batch->feed->name . ' added';
            $description = $feed_stock_batch->feed->name . "'s feed with batch no.: ($feed_stock_batch->batch_no) was added by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($feed_stock_batch);
        }
        return $this->index();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(FeedStockBatch $feed_stock_batch)
    {
        $feed_stock_batch =  $feed_stock_batch->with(['liveStock.category', 'feed', 'stockOfficer', 'confirmer'])->find($feed_stock_batch->id);
        return response()->json(compact('feed_stock_batch'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, FeedStockBatch $feed_stock_batch)
    {
        //
        $user = $this->getCurrentUser();
        if ($feed_stock_batch->confirmed_by === null) {
            # code...

            $feed_stock_batch->livestock_id = $request->livestock_id;
            $feed_stock_batch->feed_id = $request->feed_id;
            $feed_stock_batch->quantity_stocked = $request->quantity_stocked;
            $feed_stock_batch->balance = $request->quantity_stocked;
            $feed_stock_batch->rate = $request->rate;
            $feed_stock_batch->amount = $request->rate * $request->quantity_stocked;
            $feed_stock_batch->net_weight = $request->net_weight;
            $feed_stock_batch->package_type = $request->package_type;
            $feed_stock_batch->unit_of_measurement = $request->unit_of_measurement;
            if ($feed_stock_batch->save()) {
                // log this activity
                $title = 'Details of feeds in stock for ' . $feed_stock_batch->feed->name . ' modified';
                $description = $feed_stock_batch->feed->name . "'s feed details with batch no.: ($feed_stock_batch->batch_no) was modified by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($feed_stock_batch);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmStock(Request $request, FeedStockBatch $feed_stock_batch)
    {
        //
        $user = $this->getCurrentUser();
        $feed_stock_batch->confirmed_by = $user->id;
        $feed_stock_batch->save();

        $this->fillUnsyncTable('feed_stock_batches', $feed_stock_batch->id);
        // log this activity
        $title = 'Feed batch information confirmed';
        $description = $feed_stock_batch->feed->name . " with batch number: ($feed_stock_batch->batch_no) was confirmed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->show($feed_stock_batch);
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(FeedStockBatch $feed_stock_batch)
    {
        if ($feed_stock_batch->confirmed_by === null) {
            $user = $this->getCurrentUser();
            $title = 'Details of feeds in stock for ' . $feed_stock_batch->feed->name . ' deleted';
            $description = $feed_stock_batch->feed->name . "'s feed details with batch no.: ($feed_stock_batch->batch_no) was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);

            $feed_stock_batch->delete();
            return response()->json([], 204);
        }
        $message = 'Record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
