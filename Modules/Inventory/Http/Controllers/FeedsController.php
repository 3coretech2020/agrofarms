<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\LiveStockFeed;

class FeedsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $livestock_feeds = LiveStockFeed::with('liveStock.category')->where('company_id', $company_id)->orderBy('name')->get();
        return response()->json(compact('livestock_feeds'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $company_id = $this->getUserCompany();
        $names = explode(',', $request->names);
        $livestock_id = $request->livestock_id;
        foreach ($names as $name) {
            $livestock_feed = LiveStockFeed::where('name', trim($name))->first();
            if (!$livestock_feed) {
                $livestock_feed = new LiveStockFeed();
                $livestock_feed->company_id = $company_id;
                $livestock_feed->name = trim($name);
                $livestock_feed->livestock_id = $livestock_id;
                $livestock_feed->save();
                $this->fillUnsyncTable('live_stock_feeds', $livestock_feed->id);
            }
        }

        return $this->index();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(LiveStockFeed $livestock_feed)
    {
        $livestock_feed =  $livestock_feed->with(['liveStock.category'])->find($livestock_feed->id);
        return response()->json(compact('livestock_feed'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStockFeed $livestock_feed)
    {
        //
        $livestock_feed->name = $request->name;
        $livestock_feed->livestock_id = $request->livestock_id;

        if ($livestock_feed->save()) {
            return $this->show($livestock_feed);
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStockFeed $livestock_feed)
    {
        $livestock_feed->delete();
        return response()->json([], 204);
    }
}
