<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\Manure;

class ManuresController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $fetilizers = Manure::with('lastStocker')->where('company_id', $company_id)->get();
        return response()->json(compact('fetilizers'), 200);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $type = $request->type;
        $manure = Manure::where(['company_id' => $company_id, 'type' => $type])->first();
        if (!$manure) {
            $manure = new Manure();
            $manure->company_id = $company_id;
            $manure->type = $type;
            $manure->quantity_in_stock = $request->quantity;
            $manure->sold = 0;
            $manure->balance = $request->quantity;
            $manure->package_type = 'bags';
            $manure->last_stocker = $user->id;
            $manure->last_quantity_stocked = $request->quantity;
        } else {

            $manure->quantity_in_stock += $request->quantity;
            $manure->balance += $request->quantity;
            $manure->last_stocker = $user->id;
            $manure->last_quantity_stocked = $request->quantity;
        }
        if ($manure->save()) {
            // $manure->manureStock->sold += $request->quantity;
            // $manure->manureStock->balance -= $request->quantity;
            // $manure->manureStock->save();
            $this->fillUnsyncTable('manures', $manure->id);
            // log this activity
            $title = 'Bag of organic manure added';
            $description = $request->quantity . ' bags of ' . $type . '  was added to stock by ' . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->index();
        }
        $message = 'Manure record could not be saved';
        return response()->json(compact('message'), 500);
    }
}
