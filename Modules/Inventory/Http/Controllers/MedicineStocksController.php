<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\LiveStockMedicine;
use Modules\Inventory\Entities\MedicineStockBatch;
use Modules\Livestock\Entities\LiveStock;

class MedicineStocksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $request = request()->all();
        $company_id = $this->getUserCompany();
        $livestock_id = $request['livestock_id'];
        $medicines_in_stock = MedicineStockBatch::with(['liveStock.category', 'medicine', 'stockOfficer', 'confirmer'])->where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->orderBy('batch_no', 'DESC')->get();
        return response()->json(compact('medicines_in_stock'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function fetchLivestockMedicines($livestock_id)
    {
        $medicines = LiveStockMedicine::where('livestock_id', $livestock_id)->get();
        return response()->json(compact('medicines'), 200);
    }

    public function fetchBatches($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $medicine_stock_batches =
            MedicineStockBatch::groupBy('medicine_id')->with('medicine')->where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->where('balance', '>', 0)->where('confirmed_by', '!=', null)->select('*', \DB::raw('SUM(balance) as balance'))->orderBy('batch_no')->get();
        return response()->json(compact('medicine_stock_batches'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $prefix = 'MD-';
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $medicine_stock_batch = new MedicineStockBatch();
        $medicine_stock_batch->company_id = $company_id;;
        $medicine_stock_batch->livestock_id = $request->livestock_id;
        $medicine_stock_batch->medicine_id = $request->medicine_id;
        $medicine_stock_batch->quantity_stocked = $request->quantity_stocked;
        $medicine_stock_batch->balance = $request->quantity_stocked;
        $medicine_stock_batch->stocked_by = $user->id;
        $medicine_stock_batch->rate = $request->rate;
        $medicine_stock_batch->amount = $request->rate * $request->quantity_stocked;
        $medicine_stock_batch->net_weight = $request->net_weight;
        $medicine_stock_batch->package_type = $request->package_type;
        $medicine_stock_batch->unit_of_measurement = $request->unit_of_measurement;

        if ($medicine_stock_batch->save()) {

            $medicine_stock_batch->batch_no = $this->getBatchNo($prefix, $medicine_stock_batch->id);
            $medicine_stock_batch->save();

            // log this activity
            $title = 'New Batch of Medicines for ' . $medicine_stock_batch->medicine->name . ' added';
            $description = $medicine_stock_batch->medicine->name . "'s medicine with batch no.: ($medicine_stock_batch->batch_no) was added by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($medicine_stock_batch);
        }
        return $this->index();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(MedicineStockBatch $medicine_stock_batch)
    {
        $medicine_stock_batch =  $medicine_stock_batch->with(['liveStock.category', 'medicine', 'stockOfficer', 'confirmer'])->find($medicine_stock_batch->id);
        return response()->json(compact('medicine_stock_batch'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, MedicineStockBatch $medicine_stock_batch)
    {
        //
        $user = $this->getCurrentUser();
        if ($medicine_stock_batch->confirmed_by === null) {
            # code...

            $medicine_stock_batch->livestock_id = $request->livestock_id;
            $medicine_stock_batch->medicine_id = $request->medicine_id;
            $medicine_stock_batch->quantity_stocked = $request->quantity_stocked;
            $medicine_stock_batch->balance = $request->quantity_stocked;
            $medicine_stock_batch->rate = $request->rate;
            $medicine_stock_batch->amount = $request->rate * $request->quantity_stocked;
            $medicine_stock_batch->net_weight = $request->net_weight;
            $medicine_stock_batch->package_type = $request->package_type;
            $medicine_stock_batch->unit_of_measurement = $request->unit_of_measurement;
            if ($medicine_stock_batch->save()) {
                // log this activity
                $title = 'Details of medicines in stock for ' . $medicine_stock_batch->medicine->name . ' modified';
                $description = $medicine_stock_batch->medicine->name . "'s medicine details with batch no.: ($medicine_stock_batch->batch_no) was modified by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($medicine_stock_batch);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmStock(Request $request, MedicineStockBatch $medicine_stock_batch)
    {
        //
        $user = $this->getCurrentUser();
        $medicine_stock_batch->confirmed_by = $user->id;
        $medicine_stock_batch->save();
        $this->fillUnsyncTable('medicine_stock_batches', $medicine_stock_batch->id);
        // log this activity
        $title = 'Medicine batch information confirmed';
        $description = $medicine_stock_batch->medicine->name . " with batch number: ($medicine_stock_batch->batch_no) was confirmed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->show($medicine_stock_batch);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(MedicineStockBatch $medicine_stock_batch)
    {
        if ($medicine_stock_batch->confirmed_by === null) {
            $user = $this->getCurrentUser();
            $title = 'Details of medicines in stock for ' . $medicine_stock_batch->medicine->name . ' deleted';
            $description = $medicine_stock_batch->medicine->name . "'s medicine details with batch no.: ($medicine_stock_batch->batch_no) was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);

            $medicine_stock_batch->delete();
            return response()->json([], 204);
        }
        $message = 'Record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
