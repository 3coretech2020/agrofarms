<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\LiveStockMedicine;

class MedicinesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $livestock_medicines = LiveStockMedicine::with('liveStock.category')->where('company_id', $company_id)->orderBy('name')->get();
        return response()->json(compact('livestock_medicines'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $company_id = $this->getUserCompany();
        $names = explode(',', $request->names);
        $livestock_id = $request->livestock_id;
        foreach ($names as $name) {
            $livestock_medicine = LiveStockMedicine::where('name', trim($name))->first();
            if (!$livestock_medicine) {
                $livestock_medicine = new LiveStockMedicine();
                $livestock_medicine->company_id = $company_id;
                $livestock_medicine->name = trim($name);
                $livestock_medicine->livestock_id = $livestock_id;
                $livestock_medicine->save();

                $this->fillUnsyncTable('live_stock_medicines', $livestock_medicine->id);
            }
        }

        return $this->index();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(LiveStockMedicine $livestock_medicine)
    {
        $livestock_medicine =  $livestock_medicine->with(['liveStock.category'])->find($livestock_medicine->id);
        return response()->json(compact('livestock_medicine'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStockMedicine $livestock_medicine)
    {
        //
        $livestock_medicine->name = $request->name;
        $livestock_medicine->livestock_id = $request->livestock_id;


        if ($livestock_medicine->save()) {
            return $this->show($livestock_medicine);
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStockMedicine $livestock_medicine)
    {
        //
        $livestock_medicine->delete();
        return response()->json([], 204);
    }
}
