/** When your routing table is too long, you split it into small modules**/
import Layout from '@/layout';

const inventoryRoutes = {
  path: '/inventory',
  component: Layout,
  redirect: '/inventory/feed-cost',
  name: 'Inventory',
  alwaysShow: true,
  meta: {
    title: 'Inventory',
    icon: 'el-icon-price-tag',
  },
  children: [
    {
      component: () => import ('./views/Eggs'),
      path: '/inventory/eggs',
      name: 'Eggs',
      meta: {
        title: 'Eggs',
        // permissions: ['stock eggs'],
      },
    },
    {
      component: () => import ('./views/BrokenEggs'),
      path: '/inventory/broken-eggs',
      name: 'BrokenEggs',
      meta: {
        title: 'Broken Eggs',
        // permissions: ['stock eggs'],
      },
    },
    {
      component: () => import ('./views/Feeds'),
      path: '/inventory/feeds',
      name: 'LiveStockFeeds',
      meta: {
        title: 'Feeds',
        // permissions: ['manage feeds'],
      },
    },
    {
      component: () => import ('./views/Medicines'),
      path: '/inventory/medicines',
      name: 'LiveStockMedicine',
      meta: {
        title: 'Medicine',
        // permissions: ['manage medicines'],
      },
    },
    {
      component: () => import ('./views/FeedStock'),
      // component: () => import ('@modules/Account/Resources/assets/js/views/income/index.vue'),
      path: '/inventory/feed-in-stock',
      name: 'FeedStock',
      meta: {
        title: 'Feeds In Stock',
      },
    },
    {
      component: () => import ('./views/MedicineStock'),
      // component: () => import ('@modules/Account/Resources/assets/js/views/income/index.vue'),
      path: '/inventory/medicine-in-cost',
      name: 'MedicineStock',
      meta: {
        title: 'Medicines In Stock',
      },
    },
    {
      component: () => import ('./views/Fertilizers'),
      // component: () => import ('@modules/Account/Resources/assets/js/views/income/index.vue'),
      path: '/inventory/fertilizers',
      name: 'Fertilizers',
      meta: {
        title: 'Fertilizers',
      },
    },
  ],
};

export default inventoryRoutes;
