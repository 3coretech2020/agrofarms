<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {


    Route::prefix('inventory')->group(function () {
        //////////////livestockfeed Routes/////////////////
        Route::prefix('feeds')->group(function () {
            Route::get('/', 'FeedsController@index');
            Route::post('/store', 'FeedsController@store');
            Route::put('/update/{livestock_feed}', 'FeedsController@update');
            Route::delete('/destroy/{livestock_feed}', 'FeedsController@destroy');
        });
        ///////////livestockmedicine Routes//////////////////
        Route::prefix('medicines')->group(function () {
            Route::get('/', 'MedicinesController@index');
            Route::post('/store', 'MedicinesController@store');
            Route::put('/update/{livestock_medicine}', 'MedicinesController@update');
            Route::delete('/destroy/{livestock_medicine}', 'MedicinesController@destroy');
        });
        Route::prefix('feed-stock')->group(function () {
            Route::get('/', 'FeedStocksController@index');
            Route::post('/store', 'FeedStocksController@store');
            Route::put('/update/{feed_stock_batch}', 'FeedStocksController@update');
            Route::delete('/destroy/{feed_stock_batch}', 'FeedStocksController@destroy');
            Route::get('/fetch-feeds/{livestock_id}', 'FeedStocksController@fetchLivestockFeeds');
            Route::put('/confirm-stock/{feed_stock_batch}', 'FeedStocksController@confirmStock');
            Route::get('/fetch-batches/{livestock_id}', 'FeedStocksController@fetchBatches');
        });
        Route::prefix('medicine-stock')->group(function () {
            Route::get('/', 'MedicineStocksController@index');
            Route::post('/store', 'MedicineStocksController@store');
            Route::put('/update/{medicine_stock_batch}', 'MedicineStocksController@update');
            Route::delete('/destroy/{medicine_stock_batch}', 'MedicineStocksController@destroy');
            Route::get('/fetch-medicines/{livestock_id}', 'MedicineStocksController@fetchLivestockMedicines');
            Route::put('/confirm-stock/{medicine_stock_batch}', 'MedicineStocksController@confirmStock');

            Route::get('/fetch-batches/{livestock_id}', 'MedicineStocksController@fetchBatches');
        });
        Route::prefix('egg-stock')->group(function () {
            Route::get('/', 'EggStocksController@index');
            Route::post('/store', 'EggStocksController@store');
            Route::put('/update/{egg_stock}', 'EggStocksController@update');
            Route::delete('/destroy/{egg_stock}', 'EggStocksController@destroy');
            Route::get('/fetch-eggs/{livestock_id}', 'EggStocksController@fetchEggStocks');
            Route::put('/confirm-stock/{egg_stock}', 'EggStocksController@confirmStock');

            Route::get('/fetch-batches/{livestock_id}', 'EggStocksController@fetchLivestockBatches');

            Route::get('/broken-eggs', 'EggStocksController@allBrokenEggs');
            Route::post('/save-broken-eggs', 'EggStocksController@saveBrokenEggs');
            Route::put('/update-broken-eggs/{broken_egg}', 'EggStocksController@updateBrokenEggs');
            Route::delete('/destroy-broken-eggs/{broken_egg}', 'EggStocksController@destroyBrokenEggs');
            Route::put('/confirm-broken-eggs/{broken_egg}', 'EggStocksController@confirmBrokenEggs');
        });
        Route::prefix('fertilizer')->group(function () {
            Route::get('/', 'ManuresController@index');
            Route::post('/store', 'ManuresController@store');
        });
    });
});
