<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveStockBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_stock_batches', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_id');
            $table->integer('quantity_stocked');
            $table->date('date_stocked');
            $table->integer('stocked_by');
            $table->integer('confirmed_by')->nullable();
            $table->double('rate', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_stock_batches');
    }
}
