<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveStockMortalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_stock_mortalities', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_id');
            $table->date('death_date');
            $table->string('cause_of_death');
            $table->integer('batch_id');
            $table->integer('quantity');
            $table->integer('recorded_by');
            $table->integer('confirmed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_stock_mortalities');
    }
}
