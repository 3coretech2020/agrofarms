<?php

namespace Modules\Livestock\Entities;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    // use HasFactory;

    protected $fillable = [];

    public function liveStocks()
    {
        return $this->hasMany(LiveStock::class,'category_id','id');
    }
}
