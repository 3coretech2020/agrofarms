<?php

namespace Modules\Livestock\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\FeedStockBatch;

class Feeding extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function feedBatch()
    {
        return $this->belongsTo(FeedStockBatch::class, 'feed_stock_batch_id', 'id');
    }
    public function livestockBatch()
    {
        return $this->belongsTo(LiveStockBatch::class, 'live_stock_batch_id', 'id');
    }
    public function feeder()
    {
        return $this->belongsTo(User::class, 'fed_by', 'id');
    }
}
