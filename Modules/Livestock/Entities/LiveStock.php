<?php

namespace Modules\Livestock\Entities;

use \App\Company;
use Illuminate\Database\Eloquent\Model;
use Modules\Sales\Entities\LiveStockSale;

class LiveStock extends Model
{

    protected $fillable = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function batches()
    {
        return $this->hasMany(LiveStockBatch::class, 'livestock_id', 'id');
    }
    public function feeds()
    {
        return $this->hasMany(LiveStockFeed::class, 'livestock_id', 'id');
    }
    public function medicines()
    {
        return $this->hasMany(LiveStockMedicine::class, 'livestock_id', 'id');
    }
    public function mortalities()
    {
        return $this->hasMany(LiveStockMortality::class, 'livestock_id', 'id');
    }
    public function sales()
    {
        return $this->hasMany(LiveStockSale::class, 'livestock_id', 'id');
    }
    public function costOfRearing($company_id, $livestock_batch_id)
    {
        // $company_id = $this->getUserCompany();
        $livestock_batch = LiveStockBatch::find($livestock_batch_id);
        $livestock_rate = $livestock_batch->rate;
        $remaining_livestock = $livestock_batch->quantity_stocked - $livestock_batch->quantity_dead - $livestock_batch->quantity_sold;
        $medications = Medication::groupBy(['company_id', 'medicine_stock_batch_id'])->where(['company_id' => $company_id, 'live_stock_batch_id' => $livestock_batch_id])->select('*', \DB::raw('SUM(quantity) as quantity'))->get();
        $feedings = Feeding::groupBy(['company_id', 'feed_stock_batch_id'])->where(['company_id' => $company_id, 'live_stock_batch_id' => $livestock_batch_id])->select('*', \DB::raw('SUM(quantity) as quantity'))->get();

        $total_amount_spent_on_each_livestock_for_medication = 0;
        $total_amount_spent_on_each_livestock_for_feeding = 0;
        foreach ($medications as $medication) {
            $rate =  $medication->medicineBatch->rate;
            $amount_consumed_by_batch = $medication->quantity * $rate;
            $amount_consumed_by_each_livestock = $amount_consumed_by_batch / $remaining_livestock;

            $total_amount_spent_on_each_livestock_for_medication += $amount_consumed_by_each_livestock;
        }
        foreach ($feedings as $feeding) {
            $rate =  $feeding->feedBatch->rate;
            $amount_consumed_by_batch = $feeding->quantity * $rate;
            $amount_consumed_by_each_livestock = $amount_consumed_by_batch / $remaining_livestock;

            $total_amount_spent_on_each_livestock_for_feeding += $amount_consumed_by_each_livestock;
        }

        $cost_of_rearing_each = $total_amount_spent_on_each_livestock_for_medication + $total_amount_spent_on_each_livestock_for_feeding  + $livestock_rate;

        return $cost_of_rearing_each;
    }
}
