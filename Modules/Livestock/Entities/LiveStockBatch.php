<?php

namespace Modules\Livestock\Entities;

use \App\Company;
use \App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Sales\Entities\LiveStockSale;

class LiveStockBatch extends Model
{


    protected $fillable = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function stockOfficer()
    {
        return $this->belongsTo(User::class, 'stocked_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
    public function mortalities()
    {
        return $this->hasMany(LiveStockMortality::class, 'batch_id', 'id');
    }
    public function sales()
    {
        return $this->hasMany(LiveStockSale::class, 'live_stock_batch_id', 'id');
    }
    public function medications()
    {
        return $this->hasMany(Medication::class, 'live_stock_batch_id', 'id');
    }
    public function feedings()
    {
        return $this->hasMany(Feeding::class, 'live_stock_batch_id', 'id');
    }
}
