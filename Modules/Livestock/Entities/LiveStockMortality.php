<?php

namespace Modules\Livestock\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;


class LiveStockMortality extends Model
{


    protected $fillable = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function batch()
    {
        return $this->belongsTo(LiveStockBatch::class, 'batch_id', 'id');
    }
    public function recorder()
    {
        return $this->belongsTo(User::class, 'recorded_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
