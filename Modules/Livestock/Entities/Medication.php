<?php

namespace Modules\Livestock\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\MedicineStockBatch;

class Medication extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function medicineBatch()
    {
        return $this->belongsTo(MedicineStockBatch::class, 'medicine_stock_batch_id', 'id');
    }
    public function livestockBatch()
    {
        return $this->belongsTo(LiveStockBatch::class, 'live_stock_batch_id', 'id');
    }
    public function administeredBy()
    {
        return $this->belongsTo(User::class, 'action_by', 'id');
    }
}
