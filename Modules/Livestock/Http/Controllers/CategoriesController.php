<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Livestock\Entities\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $categories = Category::get();
        if ($categories->count() < 1 ) {
            $message = 'Not Found';
            return response()->json(compact('message'), 404);
        }
        return response()->json(compact('categories'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $name = $request->name;
        $category = new Category();
        $category->name = $name;
        if ($category->save()) {
            $message = $name .' was saved successful';
            return response()->json(compact('message'), 200);
        }
        $message = $name .' could not be saved';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('livestock::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Category $category)
    {
        //
       // $category = Category::find($id);
        $former_name = $category->name;
        $name = $request->name;
        $category->name  = $name;
        if ($category->save()) {
            $message = $former_name .' was updated to '. $name;
            return response()->json(compact('message'), 200);
        }
        $message = $former_name .' could not be updated to '. $name;
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Category $category)
    {
        //
        $former_name = $category->name;
        $category->delete();
        $message = $former_name .' was deleted';
        return response()->json(compact('message'), 200);
    }
}
