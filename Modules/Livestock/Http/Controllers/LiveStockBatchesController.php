<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Livestock\Entities\Category;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;

class LiveStockBatchesController extends Controller
{
    public function index()
    {
        $company_id = $this->getUserCompany();
        $live_stock_batches = LiveStockBatch::with(['liveStock.category', 'stockOfficer', 'confirmer', 'mortalities', 'sales'])->where('company_id', $company_id)->get();
        // if ($live_stock_batches->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('live_stock_batches'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $livestock = LiveStock::find($request->livestock_id);
        $prefix = strtolower(str_replace(' ', '', $livestock->name)) . '-';
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $fetch_next_batch = LiveStockBatch::where('batch_no', 'like', '%~' . $prefix . '~%')->orderBy('id', 'DESC')->first();
        $next_batch_id = 1;
        if ($fetch_next_batch) {
            $next_batch_id = $fetch_next_batch->next_batch;
        }
        $livestockbatch = new LiveStockBatch();
        $livestockbatch->company_id = $company_id;;
        $livestockbatch->livestock_id = $request->livestock_id;
        $livestockbatch->stocked_age_in_days = $request->stocked_age_in_days;
        $livestockbatch->quantity_stocked = $request->quantity_stocked;
        $livestockbatch->date_stocked = $request->date_stocked;
        $livestockbatch->stocked_by = $user->id;
        $livestockbatch->rate = $request->rate;

        if ($livestockbatch->save()) {

            $livestockbatch->batch_no = $this->getBatchNo($prefix, $next_batch_id);
            $livestockbatch->next_batch = $next_batch_id++;
            $livestockbatch->save();

            // log this activity
            $title = 'New Batch of ' . $livestock->name . ' added';
            $description = $livestock->name . " with batch number: ($livestockbatch->batch_no) was added by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($livestockbatch);
        }
        $message = 'New Livestock Batch could not be saved';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(LiveStockBatch $live_stock_batch)
    {
        $live_stock_batch =  $live_stock_batch->with(['liveStock.category', 'stockOfficer', 'confirmer', 'mortalities', 'sales', 'medications.medicineBatch', 'feedings.feedBatch'])->find($live_stock_batch->id);
        return response()->json(compact('live_stock_batch'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStockBatch $livestockbatch)
    {
        //
        if ($livestockbatch->confirmed_by === null) {
            $user = $this->getCurrentUser();
            $livestockbatch->livestock_id = $request->livestock_id;
            $livestockbatch->rate = $request->rate;
            $livestockbatch->stocked_age_in_days = $request->stocked_age_in_days;
            $livestockbatch->quantity_stocked = $request->quantity_stocked;
            $livestockbatch->date_stocked = $request->date_stocked;

            if ($livestockbatch->save()) {
                // log this activity
                $title = 'Livestock batch information updated';
                $description = $livestockbatch->liveStock->name . " with batch number: ($livestockbatch->batch_no) was updated by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($livestockbatch);
            }
        }
        $message = 'Update action failed';
        return response()->json(compact('message'), 500);
    }
    public function confirmBatch(Request $request, LiveStockBatch $batch)
    {
        //
        $user = $this->getCurrentUser();
        $batch->confirmed_by = $user->id;
        $batch->save();

        $this->fillUnsyncTable('live_stock_batches', $batch->id);
        // log this activity
        $title = 'Livestock batch information confirmed';
        $description = $batch->liveStock->name . " with batch number: ($batch->batch_no) was confirmed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->show($batch);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStockBatch $livestockbatch)
    {
        if ($livestockbatch->confirmed_by === null) {
            $user = $this->getCurrentUser();

            // log this activity
            $title = 'Livestock batch information deleted';
            $description = $livestockbatch->liveStock->name . " with batch number: ($livestockbatch->batch_no) was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];

            $livestockbatch->delete();
            $this->logUserActivity($title, $description, $roles);
            return response()->json([], 204);
        }
        $message = 'Delete action failed';
        return response()->json(compact('message'), 500);
    }
}
