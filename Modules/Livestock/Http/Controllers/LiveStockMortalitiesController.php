<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Livestock\Entities\LiveStockBatch;
use Modules\Livestock\Entities\LiveStockMortality;

class LiveStockMortalitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $livestock_mortalities = LiveStockMortality::with(['liveStock.category', 'batch', 'recorder', 'confirmer'])->where('company_id', $company_id)->get();
        return response()->json(compact('livestock_mortalities'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $company_id = $this->getUserCompany();
        $user = $this->getCurrentUser();

        $livestock_mortality = new LiveStockMortality();
        $livestock_mortality->company_id = $company_id;
        $livestock_mortality->livestock_id = $request->livestock_id;
        $livestock_mortality->death_date = $request->death_date;
        $livestock_mortality->cause_of_death = $request->cause_of_death;
        $livestock_mortality->batch_id = $request->batch_id;
        $livestock_mortality->quantity = $request->quantity;
        $livestock_mortality->recorded_by = $user->id;

        if ($livestock_mortality->save()) {


            $title = 'New mortality record made';
            $description = 'Mortaility record for ' . $livestock_mortality->liveStock->name . ' with batch number: ' . $livestock_mortality->batch->batch_no . ' was made by ' . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($livestock_mortality);
        }
        $message = 'Mortality record could not be saved';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(LiveStockMortality $livestock_mortality)
    {
        $livestock_mortality =  $livestock_mortality->with(['liveStock.category', 'batch', 'recorder', 'confirmer'])->find($livestock_mortality->id);
        return response()->json(compact('livestock_mortality'), 200);
    }

    public function confirmMortality(Request $request, LiveStockMortality $livestock_mortality)
    {
        //
        $user = $this->getCurrentUser();
        $livestock_mortality->confirmed_by = $user->id;
        if ($livestock_mortality->save()) {
            // let's update the quantity_dead column of the livestock_batch table
            $batch = LiveStockBatch::find($livestock_mortality->batch_id);
            $batch->quantity_dead += $request->quantity;
            $batch->save();
        }


        // log this activity
        $title = 'Mortality record confirmed';
        $description = 'Mortaility record for ' . $livestock_mortality->liveStock->name . ' with batch number: ' . $livestock_mortality->batch->batch_no . ' was confirmed by ' . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->show($livestock_mortality);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStockMortality $livestock_mortality)
    {
        if ($livestock_mortality->confirmed_by === null) {
            $user = $this->getCurrentUser();

            $difference_in_quantity = $request->quantity - $livestock_mortality->quantity;
            $livestock_mortality->livestock_id = $request->livestock_id;
            $livestock_mortality->death_date = $request->death_date;
            $livestock_mortality->cause_of_death = $request->cause_of_death;
            $livestock_mortality->batch_id = $request->batch_id;
            $livestock_mortality->quantity = $request->quantity;

            if ($livestock_mortality->save()) {
                // let's update the quantity_dead column of the livestock_batch table
                // $batch = LiveStockBatch::find($request->batch_id);
                // $batch->quantity_dead += $difference_in_quantity;
                // $batch->save();

                $title = 'Mortality record modified';
                $description = 'Mortaility record for ' . $livestock_mortality->liveStock->name . ' with batch number: ' . $livestock_mortality->batch->batch_no . ' was modified by ' . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($livestock_mortality);
            }
        }
        $message = 'Mortality record could not be updated';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStockMortality $livestock_mortality)

    {
        //
        // $old_quantity = $livestock_mortality->quantity;
        // // let's update the quantity_dead column of the livestock_batch table
        // $batch = $livestock_mortality->batch;
        // $batch->quantity_dead -= $old_quantity;
        // $batch->save();
        if ($livestock_mortality->confirmed_by === null) {
            $user = $this->getCurrentUser();

            $title = 'Mortality record deleted';
            $description = 'Mortaility record for ' . $livestock_mortality->liveStock->name . ' with batch number: ' . $livestock_mortality->batch->batch_no . ' was deleted by ' . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            $livestock_mortality->delete();
            return response()->json([], 204);
        }
        $message = 'Mortality record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
