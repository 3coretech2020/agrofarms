<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;

class LiveStocksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $live_stocks = $this->getCompanyLivestocks($company_id);

        if ($live_stocks->count() < 1) {
            $message = 'Not Found';
            return response()->json(compact('message'), 404);
        }
        return response()->json(compact('live_stocks'), 200);
    }

    // This fragment of code needs to be optimized to account for mortalities and sold out livestocks
    public function fetchLivestockBatches($livestock_id)
    {
        $company_id = $this->getUserCompany();
        $batches = LiveStockBatch::where(['livestock_id' => $livestock_id, 'company_id' => $company_id])->whereRaw('quantity_stocked - quantity_dead - quantity_sold > 0')->get();
        return response()->json(compact('batches'), 200);
    }
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */

    public function store(Request $request)
    {
        //
        $livestock = new LiveStock();
        $livestock->company_id = $request->company_id;
        $livestock->name = $request->name;
        $livestock->category_id = $request->category_id;
        $livestock->save();

        if ($livestock->save()) {
            $message = $livestock . ' was saved successful';
            return response()->json(compact('message'), 200);
        }
        $message = $livestock . ' could not be saved';
        return response()->json(compact('message'), 500);
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('livestock::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStock $livestock)
    {
        //
        $former_name = $livestock;
        $livestock->company_id = $request->company_id;
        $livestock->name = $request->name;
        $livestock->category_id = $request->category_id;


        if ($livestock->save()) {
            $message = $former_name . ' was updated to ' . $livestock;
            return response()->json(compact('message'), 200);
        }
        $message = $former_name . ' could not be updated to ' . $livestock;
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStock $livestock)
    {
        //
        $former_name = $livestock;
        $livestock->company_id;
        $livestock->category_id;
        $livestock->name;

        $livestock->delete();

        if ($livestock->save()) {
            $message = $former_name . ' was updated to ' . $livestock;
            return response()->json(compact('message'), 200);
        }
        $message = $former_name . ' could not be updated to ' . $livestock;
        return response()->json(compact('message'), 500);
    }
}
