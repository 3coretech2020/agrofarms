<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\FeedStockBatch;
use Modules\Livestock\Entities\Feeding;
use Modules\Livestock\Entities\LiveStockBatch;

class LiveStockFeedingsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $feedings = Feeding::with(['feedBatch.feed', 'livestockBatch.liveStock', 'feeder'])->where('company_id', $company_id)->orderBy('feeding_date', 'DESC')->get();
        // if ($live_stock_batches->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('feedings'), 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('livestock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $livestock_id = $request->livestock_id;
        $live_stock_batch_id = $request->live_stock_batch_id;
        $feed_id = $request->feed_id;
        $date = $request->feeding_date;
        $quantity = $request->quantity;
        $feed_stock_batches = FeedStockBatch::where(['company_id' => $company_id, 'livestock_id' => $livestock_id, 'feed_id' => $feed_id])->where('balance', '>', 0)->orderBy('id')->get();
        // we deduct the feed stock by FIFO principle
        foreach ($feed_stock_batches as $feed_stock_batch) {
            $balance = $feed_stock_batch->balance;
            if ($balance <= $quantity) {
                $feed_stock_batch->consumed += $balance;
                $feed_stock_batch->balance -= $balance;
                $feed_stock_batch->save();
                $quantity -= $balance;
                $this->saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $feed_stock_batch->id, $balance, $date, $user->id);
            } else {
                $feed_stock_batch->consumed += $quantity;
                $feed_stock_batch->balance -= $quantity;
                $feed_stock_batch->save();
                $this->saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $feed_stock_batch->id, $quantity, $date, $user->id);
                break;
            }
        }
        $livestock_batch = LiveStockBatch::find($live_stock_batch_id);
        $title = 'Livestock feeding action created';
        $description = $livestock_batch->liveStock->name . " with batch number: " . $livestock_batch->batch_no . " were fed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->index();
    }

    private function saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $feed_stock_batch_id, $quantity, $date, $fed_by)
    {
        $feeding = new Feeding();
        $feeding->company_id = $company_id;
        $feeding->livestock_id = $livestock_id;
        $feeding->live_stock_batch_id = $live_stock_batch_id;
        $feeding->feed_stock_batch_id = $feed_stock_batch_id;
        $feeding->feeding_date = $date;
        $feeding->quantity = $quantity;
        $feeding->fed_by = $fed_by;
        $feeding->save();
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Feeding $feeding)
    {
        $feeding =  $feeding->with(['feedBatch.feed', 'livestockBatch.liveStock', 'feeder'])->find($feeding->id);
        return response()->json(compact('feeding'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Feeding $feeding)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $difference = $request->quantity - $feeding->quantity;
        $feeding->livestock_id = $request->livestock_id;
        $feeding->live_stock_batch_id = $request->live_stock_batch_id;
        $feeding->feed_stock_batch_id = $request->feed_stock_batch_id;
        $feeding->quantity = $request->quantity;
        $feeding->feeding_date = $request->feeding_date;
        if ($feeding->save()) {
            // log this activity
            $feeding->feedBatch->consumed += $difference;
            $feeding->feedBatch->balance -= $difference;
            $feeding->feedBatch->save();

            $title = 'Livestock feeding action modified';
            $description = $feeding->livestockBatch->liveStock->name . " with batch number: " . $feeding->livestockBatch->batch_no . " feeding details was modified by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($feeding);
        }
        $message = 'Feeding Details Modification Failed';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Feeding $feeding)
    {
        $user = $this->getCurrentUser();

        // log this activity
        $title = 'Livestock feeding information deleted';
        $description = $feeding->livestockBatch->liveStock->name . " with batch number: " . $feeding->livestockBatch->batch_no . " feeding details was deleted by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        // reverse every deductions made from this stock initially////////
        $feeding->feedBatch->consumed -= $feeding->quantity;
        $feeding->feedBatch->balance += $feeding->quantity;
        $feeding->feedBatch->save();
        $feeding->delete();
        return response()->json([], 204);
    }
}
