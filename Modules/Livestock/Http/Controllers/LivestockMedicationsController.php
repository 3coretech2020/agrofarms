<?php

namespace Modules\Livestock\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\MedicineStockBatch;
use Modules\Livestock\Entities\LiveStockBatch;
use Modules\Livestock\Entities\Medication;

class LiveStockMedicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $medications = Medication::with(['medicineBatch.medicine', 'livestockBatch.liveStock', 'administeredBy'])->where('company_id', $company_id)->orderBy('medication_date', 'DESC')->get();
        // if ($live_stock_batches->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('medications'), 200);
    }

    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $livestock_id = $request->livestock_id;
        $live_stock_batch_id = $request->live_stock_batch_id;
        $medicine_id = $request->medicine_id;
        $date = $request->medication_date;
        $quantity = $request->quantity;
        $medicine_stock_batches = MedicineStockBatch::where(['company_id' => $company_id, 'livestock_id' => $livestock_id, 'medicine_id' => $medicine_id])->where('balance', '>', 0)->orderBy('id')->get();
        // we deduct the medicine stock by FIFO principle
        foreach ($medicine_stock_batches as $medicine_stock_batch) {
            $balance = $medicine_stock_batch->balance;
            if ($balance <= $quantity) {
                $medicine_stock_batch->consumed += $balance;
                $medicine_stock_batch->balance -= $balance;
                $medicine_stock_batch->save();
                $quantity -= $balance;
                $this->saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $medicine_stock_batch->id, $balance, $date, $user->id);
            } else {
                $medicine_stock_batch->consumed += $quantity;
                $medicine_stock_batch->balance -= $quantity;
                $medicine_stock_batch->save();
                $this->saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $medicine_stock_batch->id, $quantity, $date, $user->id);
                break;
            }
        }
        $livestock_batch = LiveStockBatch::find($live_stock_batch_id);
        $title = 'Livestock medication action created';
        $description = $livestock_batch->liveStock->name . " with batch number: " . $livestock_batch->batch_no . " were fed by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        return $this->index();
    }

    private function saveFeedingDetails($company_id, $livestock_id, $live_stock_batch_id, $medicine_stock_batch_id, $quantity, $date, $action_by)
    {
        $medication = new Medication();
        $medication->company_id = $company_id;
        $medication->livestock_id = $livestock_id;
        $medication->live_stock_batch_id = $live_stock_batch_id;
        $medication->medicine_stock_batch_id = $medicine_stock_batch_id;
        $medication->medication_date = $date;
        $medication->quantity = $quantity;
        $medication->action_by = $action_by;
        $medication->save();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    // public function store(Request $request)
    // {
    //     $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
    //     $company_id = $this->getUserCompany();
    //     $medication = new Medication();
    //     $medication->company_id = $company_id;
    //     $medication->livestock_id = $request->livestock_id;
    //     $medication->live_stock_batch_id = $request->live_stock_batch_id;
    //     $medication->medicine_stock_batch_id = $request->medicine_stock_batch_id;
    //     $medication->medication_date = $request->medication_date;
    //     $medication->quantity = $request->quantity;
    //     $medication->action_by = $user->id;
    //     if ($medication->save()) {
    //         $medication->medicineBatch->consumed += $request->quantity;
    //         $medication->medicineBatch->balance -= $request->quantity;
    //         $medication->medicineBatch->save();
    //         // log this activity
    //         $title = 'Livestock medication action created';
    //         $description = $medication->livestockBatch->liveStock->name . " with batch number: " . $medication->livestockBatch->batch_no . " were fed by " . $user->name;
    //         $roles = ['auditor', 'farm-officer'];
    //         $this->logUserActivity($title, $description, $roles);
    //         return $this->show($medication);
    //     }
    //     $message = 'Livestock Medication Action Failed';
    //     return response()->json(compact('message'), 500);
    // }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Medication $medication)
    {
        $medication =  $medication->with(['medicineBatch.medicine', 'livestockBatch.liveStock', 'administeredBy'])->find($medication->id);
        return response()->json(compact('medication'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('livestock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Medication $medication)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $difference = $request->quantity - $medication->quantity;
        $medication->livestock_id = $request->livestock_id;
        $medication->live_stock_batch_id = $request->live_stock_batch_id;
        $medication->medicine_stock_batch_id = $request->medicine_stock_batch_id;
        $medication->quantity = $request->quantity;
        $medication->medication_date = $request->medication_date;
        if ($medication->save()) {
            // log this activity
            $medication->medicineBatch->consumed += $difference;
            $medication->medicineBatch->balance -= $difference;
            $medication->medicineBatch->save();

            $title = 'Livestock medication action modified';
            $description = $medication->livestockBatch->liveStock->name . " with batch number: " . $medication->livestockBatch->batch_no . " medication details was modified by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($medication);
        }
        $message = 'Medication Details Modification Failed';
        return response()->json(compact('message'), 500);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Medication $medication)
    {
        $user = $this->getCurrentUser();

        // log this activity
        $title = 'Livestock medication information deleted';
        $description = $medication->livestockBatch->liveStock->name . " with batch number: " . $medication->livestockBatch->batch_no . " medication details was deleted by " . $user->name;
        $roles = ['auditor', 'farm-officer'];
        $this->logUserActivity($title, $description, $roles);
        // reverse every deductions made from this stock initially////////
        $medication->medicineBatch->consumed -= $medication->quantity;
        $medication->medicineBatch->balance += $medication->quantity;
        $medication->medicineBatch->save();
        $medication->delete();
        return response()->json([], 204);
    }
}
