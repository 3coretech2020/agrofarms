/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const liveStockRoutes = {
  path: '/live-stocks',
  component: Layout,
  redirect: '/live-stocks/batches',
  name: 'LiveStock',
  alwaysShow: true,
  meta: {
    title: 'Live Stock',
    icon: 'el-icon-money',
    // roles: ['admin', 'auditor'],
  },
  children: [
    // {
    //   component: () => import('./views/index'),
    //   path: '/live-stocks/index',
    //   name: 'LiveStockIndex',
    //   meta: {
    //     title: 'All Live Stocks',
    //     roles: ['admin', 'auditor'],
    //     permissions: ['manage income'],
    //   },
    // },
    {
      component: () => import('./views/Batches'),
      path: '/live-stocks/batches',
      name: 'LiveStockBatches',
      meta: {
        title: 'Batches',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage income'],
      },
    },
    {
      component: () => import('./views/Feedings'),
      path: '/live-stocks/feedings',
      name: 'LiveStockFeedings',
      meta: {
        title: 'Feedings',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage income'],
      },
    },
    {
      component: () => import('./views/Medications'),
      path: '/live-stocks/medications',
      name: 'LiveStockMedications',
      meta: {
        title: 'Medications',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage income'],
      },
    },
    {
      component: () => import ('./views/Mortalities'),
      path: '/live-stocks/mortalities',
      name: 'LiveStockMortalities',
      meta: {
        title: 'Mortalities',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
    {
      path: '/live-stocks/details/:id(\\d+)',
      component: () => import('./views/LivestockBatchDetails'),
      name: 'LivestockBatchDetails',
      meta: { title: 'Details', noCache: true },
      hidden: true,
    },
  ],
};
export default liveStockRoutes;
