<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('livestock')->group(function () {
        //////////////////////livestock Routes///////////////

        Route::get('/', 'LiveStocksController@index');
        Route::post('/store', 'LiveStocksController@store');
        Route::put('/update/{livestock}', 'LiveStocksController@update');
        Route::delete('/destroy/{livestock}', 'LiveStocksController@destroy');
        Route::get('/fetch-batches/{livestock_id}', 'LiveStocksController@fetchLivestockBatches');

        ////////////////Category Routes///////////////////////////
        Route::prefix('category')->group(function () {
            Route::get('/', 'CategoriesController@index');
            Route::post('/store', 'CategoriesController@store');
            Route::put('/update/{category}', 'CategoriesController@update');
            Route::delete('/destroy/{category}', 'CategoriesController@destroy');
        });
        ///////////////livestockbatch Routes//////////
        Route::prefix('batch')->group(function () {
            Route::get('/', 'LiveStockBatchesController@index');
            Route::post('/store', 'LiveStockBatchesController@store');
            Route::put('/update/{livestockbatch}', 'LiveStockBatchesController@update');
            Route::delete('/destroy/{livestockbatch}', 'LiveStockBatchesController@destroy');
            Route::put('/confirm-batch/{batch}', 'LiveStockBatchesController@confirmBatch');
            Route::get('/details/{live_stock_batch}', 'LiveStockBatchesController@show');
        });

        ////////////////livestockmortality Routes////////////
        Route::prefix('mortalities')->group(function () {
            Route::get('/', 'LiveStockMortalitiesController@index');
            Route::post('/store', 'LiveStockMortalitiesController@store');
            Route::put('/update/{livestock_mortality}', 'LiveStockMortalitiesController@update');
            Route::delete('/destroy/{livestock_mortality}', 'LiveStockMortalitiesController@destroy');
            Route::put('/confirm-mortality/{livestock_mortality}', 'LiveStockMortalitiesController@confirmMortality');
        });

        ////////////////livestock feeding Routes////////////
        Route::prefix('feedings')->group(function () {
            Route::get('/', 'LiveStockFeedingsController@index');
            Route::post('/store', 'LiveStockFeedingsController@store');
            Route::put('/update/{feeding}', 'LiveStockFeedingsController@update');
            Route::delete('/destroy/{feeding}', 'LiveStockFeedingsController@destroy');
        });

        ////////////////livestock medication Routes////////////
        Route::prefix('medications')->group(function () {
            Route::get('/', 'LiveStockMedicationsController@index');
            Route::post('/store', 'LiveStockMedicationsController@store');
            Route::put('/update/{medication}', 'LiveStockMedicationsController@update');
            Route::delete('/destroy/{medication}', 'LiveStockMedicationsController@destroy');
        });
    });
});
