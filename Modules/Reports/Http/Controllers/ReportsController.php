<?php

namespace Modules\Reports\Http\Controllers;

use App\Company;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Modules\Inventory\Entities\FeedStockBatch;
use Modules\Inventory\Entities\LiveStockFeed;
use Modules\Inventory\Entities\LiveStockMedicine;
use Modules\Inventory\Entities\MedicineStockBatch;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;
use Modules\Livestock\Entities\LiveStockMortality;
use Modules\Sales\Entities\EggSale;
use Modules\Sales\Entities\LiveStockSale;
use Modules\Sales\Entities\ManureSale;
use App\Notification;
use Modules\Crops\Entities\PlantingCostDetail;
use Modules\Crops\Entities\SoldCrop;
use Modules\Inventory\Entities\BrokenEgg;

class ReportsController extends Controller
{

    private function fetchExpenseReport($expenses, $panel, $date_from, $date_to)
    {
        switch ($panel) {
            case 'week':
                $year_month = date('Y-m', strtotime($date_from));
                $first_day = date('d', strtotime($date_from));
                $last_day = date('d', strtotime($date_to));
                $categories = [];
                $approved = [];
                for ($i = $first_day; $i <= $last_day; $i++) {
                    $date = $year_month . '-' . $i;
                    $categories[] = date('M d, Y', strtotime($date));
                    $daily_expense = 0;
                    foreach ($expenses as $expense) {

                        if (date('d', strtotime($expense->created_at)) == $i) {
                            $daily_expense += (float) $expense->amount;
                        }
                    }
                    $approved[] = $daily_expense;
                }
                return [$approved, $categories];
                break;
            case 'month':
                $month = date('m', strtotime($date_from));
                $year = date('Y', strtotime($date_from));
                $no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $categories = [];
                $approved = [];
                for ($i = 1; $i <= $no_of_days_in_month; $i++) {
                    $categories[] = $i;
                    $daily_expense = 0;
                    foreach ($expenses as $expense) {

                        if (date('d', strtotime($expense->created_at)) == $i) {
                            $daily_expense += (float) $expense->amount;
                        }
                    }
                    $approved[] = $daily_expense;
                }
                return [$approved, $categories];
                break;
            case 'quarter':
                $month = date('m', strtotime($date_from));
                $year = date('Y', strtotime($date_from));
                $first_month = date('m', strtotime($date_from));
                $last_month = date('m', strtotime($date_to));
                $month_key = date('M', strtotime($date_from));
                $categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']; //$this->monthsInAQuarter($month_key);
                $approved = [];
                for ($i = 1; $i <= 12; $i++) {
                    $categories[] = $i;
                    $daily_expense = 0;
                    foreach ($expenses as $expense) {

                        if (date('m', strtotime($expense->created_at)) == $i) {
                            $daily_expense += (float) $expense->amount;
                        }
                    }
                    $approved[] = $daily_expense;
                }
                return [$approved, $categories];
                break;
            case 'year':
                $month = date('m', strtotime($date_from));
                $year = date('Y', strtotime($date_from));
                $no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                $approved = [];
                for ($i = 1; $i <= 12; $i++) {
                    $daily_expense = 0;
                    foreach ($expenses as $expense) {

                        if (date('m', strtotime($expense->created_at)) == $i) {
                            $daily_expense += (float) $expense->amount;
                        }
                    }
                    $approved[] = $daily_expense;
                }
                return [$approved, $categories];
                break;
            default:
                # code...
                break;
        }
    }
    public function incomeAndExpenses(Request $request)
    {
        $company_id = $this->getUserCompany();
        $date_from = Carbon::now()->startOfMonth();
        $date_to = Carbon::now()->endOfMonth();
        $panel = 'month';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        /////////////////////////////Income////////////////////////////////////////////
        $egg_sales = EggSale::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->orderBy('id', 'DESC')->get();
        $total_income = 0;
        $total_expenses = 0;
        foreach ($egg_sales as $egg_sale) {
            $egg_sale->description = 'Sale of ' . $egg_sale->quantity_in_crates . ' crate(s) of eggs';
            $egg_sale->amount = $egg_sale->total_amount;
            $egg_sale->rate = $egg_sale->rate;
            $egg_sale->farm_type = 'Livestock';
            $total_income += $egg_sale->total_amount;
        }
        $livestock_sales = LiveStockSale::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->orderBy('id', 'DESC')->get();
        foreach ($livestock_sales as $livestock_sale) {
            $livestock_sale->description = 'Sale of ' . $livestock_sale->quantity . ' ' . $livestock_sale->liveStock->name;
            $livestock_sale->amount = $livestock_sale->amount;
            $livestock_sale->rate = $livestock_sale->amount / ($livestock_sale->quantity > 0) ? $livestock_sale->quantity : 1;
            $livestock_sale->farm_type = 'Livestock';
            $total_income += $livestock_sale->amount;
        }
        $manure_sales = ManureSale::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->orderBy('id', 'DESC')->get();
        foreach ($manure_sales as $manure_sale) {
            $manure_sale->description = 'Sale of ' . $manure_sale->quantity . ' bags of ' . $manure_sale->manure->type;
            $manure_sale->amount = $manure_sale->total_amount;
            $manure_sale->rate = $manure_sale->rate;
            $manure_sale->farm_type = 'Livestock';
            $total_income += $manure_sale->amount;
        }
        $crop_sales = SoldCrop::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)/*->where('confirmed_by', '!=', null)*/->orderBy('id', 'DESC')->get();
        foreach ($crop_sales as $crop_sale) {
            $crop_sale->description = $crop_sale->description;
            $crop_sale->amount = $crop_sale->amount;
            $crop_sale->rate = $crop_sale->rate;
            $crop_sale->farm_type = 'Crops';
            $total_income += $crop_sale->amount;
        }
        $incomes = $egg_sales->mergeRecursive($livestock_sales);
        $incomes = $incomes->mergeRecursive($manure_sales);
        $incomes = $incomes->mergeRecursive($crop_sales);

        /////////////////////////////////Expenses///////////////////////////////////////////
        $feed_stocks = FeedStockBatch::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->orderBy('id', 'DESC')->get();
        foreach ($feed_stocks as $feed_stock) {
            $feed_stock->description = 'Cost of ' . $feed_stock->feed->name . ' feeds for ' . $feed_stock->liveStock->name;
            $feed_stock->amount = $feed_stock->amount;
            $feed_stock->rate = $feed_stock->rate;
            $feed_stock->farm_type = 'Livestock';
            $total_expenses += $feed_stock->amount;
        }
        $medicine_stocks = MedicineStockBatch::where('company_id', $company_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->orderBy('id', 'DESC')->get();
        foreach ($medicine_stocks as $medicine_stock) {
            $medicine_stock->description = 'Cost of ' . $medicine_stock->medicine->name . ' medicine for ' . $medicine_stock->liveStock->name;
            $medicine_stock->amount = $medicine_stock->amount;
            $medicine_stock->rate = $medicine_stock->rate;
            $medicine_stock->farm_type = 'Livestock';
            $total_expenses += $medicine_stock->amount;
        }
        $planting_costs = PlantingCostDetail::/*where('company_id', $company_id)->*/where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)/*->where('confirmed_by', '!=', null)*/->orderBy('id', 'DESC')->get();
        foreach ($planting_costs as $planting_cost) {
            $planting_cost->description = $planting_cost->description . ' (for ' . $planting_cost->plantCost->crop->name . ')';
            $planting_cost->farm_type = 'Crops';
            $total_expenses += $planting_cost->amount;
        }
        $expenses = $feed_stocks->mergeRecursive($medicine_stocks);
        $expenses = $expenses->mergeRecursive($planting_costs);

        $total_profit = $total_income - $total_expenses;


        $livestock_obj = new LiveStock();
        $mortalities = LiveStockMortality::where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->get();
        $total_losses = 0;
        foreach ($mortalities as $mortality) {
            $livestock_batch_id = $mortality->batch_id;
            $cost_of_rearing_each = $livestock_obj->costOfRearing($company_id, $livestock_batch_id);
            $total_losses += $cost_of_rearing_each * $mortality->quantity;
            $mortality->amount = $cost_of_rearing_each * $mortality->quantity;
            $mortality->rate = $cost_of_rearing_each;
        }

        $broken_eggs = BrokenEgg::where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->where('confirmed_by', '!=', null)->get();
        foreach ($broken_eggs as $broken_egg) {
            $average_cost_of_an_egg = EggSale::where('confirmed_by', '!=', null)->avg('rate');
            if ($average_cost_of_an_egg < 1) {
                $average_cost_of_an_egg = 50; // the current value of an egg in the market
            }
            $total_losses += $average_cost_of_an_egg * $broken_egg->quantity;
            $broken_egg->amount = $average_cost_of_an_egg * $broken_egg->quantity;
            $broken_egg->rate = $average_cost_of_an_egg;
        }
        $losses = $mortalities->mergeRecursive($broken_eggs);
        $data_summary = ['total_income' => $total_income, 'total_expenses' => $total_expenses, 'total_profit' => $total_profit, 'total_losses' => $total_losses];

        list($income_chart_data, $categories) = $this->fetchExpenseReport($incomes, $panel, $date_from, $date_to);
        list($expenses_chart_data, $categories) = $this->fetchExpenseReport($expenses, $panel, $date_from, $date_to);
        list($losses_chart_data, $categories) = $this->fetchExpenseReport($losses, $panel, $date_from, $date_to);
        $series = [
            [
                'name' => 'Income',
                'lineWidth' => 4,
                'color' => '#00c0ef',
                'data' => $income_chart_data,
            ],
            [
                'name' => 'Expenses',
                'lineWidth' => 4,
                'color' => '#d81b60',
                'data' => $expenses_chart_data,
            ],
            [
                'name' => 'Losses',
                'lineWidth' => 4,
                'color' => '#111111',
                'data' => $losses_chart_data,
            ],
        ];
        $plot_band = [];
        if ($panel === 'quarter' || $panel === 'year') {
            $plot_band = [
                [
                    'color' => '#fce7e734',
                    'from' => -0.5,
                    'to' => 2.5,
                    'label' => ['text' => '1st Quarter']
                ],
                [
                    'color' => '#e8c1ff34',
                    'from' => 2.5,
                    'to' => 5.5,
                    'label' => ['text' => '2nd Quarter']
                ],
                [
                    'color' => '#f7f58634',
                    'from' => 5.5,
                    'to' => 8.5,
                    'label' => ['text' => '3rd Quarter']
                ],
                [
                    'color' => '#c2e3f734',
                    'from' => 8.5,
                    'to' => 11.5,
                    'label' => ['text' => '4th Quarter']
                ],
            ];
        }
        $extra_title = ' from ' . date('M d, Y', strtotime($date_from)) . ' to ' . date('M d, Y', strtotime($date_to));
        $subtitle = strtoupper($panel . 'ly Report');
        $line_chart_details = [
            'categories'    => $categories,
            'series'      => $series,
            'title' => 'Profit and Loss Account '  . $extra_title,
            'subtitle' => $subtitle,
            'plot_band' => $plot_band
        ];
        return response()->json(compact('incomes', 'expenses', 'data_summary', 'line_chart_details'), 200);



        // return view('account::index');
    }
    public function livestocksInStock(Request $request)
    {
        $company_id = $this->getUserCompany();
        $company = Company::find($company_id);
        $date_from = Carbon::now()->startOfMonth();
        $date_to = Carbon::now()->endOfMonth();
        $panel = 'month';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        $categories = [];
        $quantity = [];
        $dead = [];
        $balance = [];
        $sold = [];

        $livestocks = $this->getCompanyLivestocks($company_id);
        foreach ($livestocks as $livestock) {
            $livestock_in_stock = LiveStockBatch::groupBy('livestock_id')->where(['company_id' => $company_id, 'livestock_id' => $livestock->id])->select('*', \DB::raw('SUM(quantity_stocked) as quantity_stocked'), \DB::raw('SUM(quantity_dead) as quantity_dead'), \DB::raw('SUM(quantity_sold) as quantity_sold'), \DB::raw('SUM(quantity_stocked) - SUM(quantity_dead) - SUM(quantity_sold) as balance'))->where('company_id', $company_id)/*->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)*/->where('confirmed_by', '!=', null)->first();

            $livestock_name = $livestock->name;
            $categories[] = $livestock_name;

            $total_stocked = 0;
            $quantity_dead = 0;
            $total_balance = 0;
            $quantity_sold = 0;
            if ($livestock_in_stock) {
                $total_stocked = $livestock_in_stock->quantity_stocked;
                $quantity_dead = $livestock_in_stock->quantity_dead;
                $total_balance = $livestock_in_stock->balance;
                $quantity_sold = $livestock_in_stock->quantity_sold;
            }
            $quantity[] = [
                'name' => $livestock_name,
                'y' => (int) $total_stocked,
            ];
            $dead[] = [
                'name' => $livestock_name,
                'y' => (int) $quantity_dead,
            ];

            $balance[] = [
                'name' => $livestock_name,
                'y' => (int) $total_balance,
            ];
            $sold[] = [
                'name' => $livestock_name,
                'y' => (int) $quantity_sold,
            ];
        }
        $series = [
            [
                'name' => 'Total Stock',
                'data' => $quantity, //array format
                // 'color' => '#333333',
                //'stack' => 'Initial Stock'
            ],
            [
                'name' => 'Sold',
                'data' => $sold, //array format
                'color' => '#f39c12',
                //'stack' => 'Sold'
            ],
            [
                'name' => 'Available',
                'data' => $balance, //array format
                'color' => '#00a65a',
                //'stack' => 'Available'
            ],
            [
                'name' => 'Dead',
                'data' => $dead, //array format
                'color' => '#DC143C',
                //'stack' => 'Dead'
            ],
        ];
        $extra_title = ' from ' . date('M d, Y', strtotime($date_from)) . ' to ' . date('M d, Y', strtotime($date_to));
        $subtitle = strtoupper($panel . 'ly Report');
        return response()->json(
            [

                'categories'    => $categories,
                'series'      => $series,
                'title' => 'Number of Livestock Available at ' . $company->name . $extra_title,
                'subtitle' => $subtitle,

            ],
            200
        );
    }
    public function feedsInStock(Request $request)
    {
        $company_id = $this->getUserCompany();
        $company = Company::find($company_id);
        $date_from = Carbon::now()->startOfMonth();
        $date_to = Carbon::now()->endOfMonth();
        $panel = 'month';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        $categories = [];
        $quantity = [];
        $consumed = [];
        $balance = [];

        $livestock_feeds = LiveStockFeed::where('company_id', $company_id)->get();
        foreach ($livestock_feeds as $livestock_feed) {
            $feed_in_stock = FeedStockBatch::groupBy('feed_id')->where(['company_id' => $company_id, 'feed_id' => $livestock_feed->id])->select('*', \DB::raw('SUM(quantity_stocked) * net_weight as quantity_stocked'), \DB::raw('SUM(consumed) * net_weight as consumed'), \DB::raw('SUM(balance) * net_weight as balance'))->where('company_id', $company_id)/*->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)*/->where('confirmed_by', '!=', null)->first();


            $unit_of_measurement = '';
            $total_stocked = 0;
            $quantity_consumed = 0;
            $total_balance = 0;
            if ($feed_in_stock) {
                $unit_of_measurement = ' (' . $feed_in_stock->unit_of_measurement . ')';
                $total_stocked = $feed_in_stock->quantity_stocked;
                $quantity_consumed = $feed_in_stock->consumed;
                $total_balance = $feed_in_stock->balance;
            }
            $feed_name = $livestock_feed->name . $unit_of_measurement;
            $categories[] = $feed_name;
            $quantity[] = [
                'name' => $feed_name,
                'y' => (int) $total_stocked,
            ];
            $consumed[] = [
                'name' => $feed_name,
                'y' => (int) $quantity_consumed,
            ];

            $balance[] = [
                'name' => $feed_name,
                'y' => (int) $total_balance,
            ];
        }
        $series = [
            [
                'name' => 'Total Stock',
                'data' => $quantity, //array format
                // 'color' => '#333333',
                //'stack' => 'Initial Stock'
            ],
            [
                'name' => 'Consumed',
                'data' => $consumed, //array format
                'color' => '#f39c12',
                //'stack' => 'Sold'
            ],
            [
                'name' => 'Balance',
                'data' => $balance, //array format
                'color' => '#00a65a',
                //'stack' => 'Available'
            ],
            // [
            //     'name' => 'Dead',
            //     'data' => $dead, //array format
            //     'color' => '#DC143C',
            //     //'stack' => 'Dead'
            // ],
        ];
        $extra_title = ' from ' . date('M d, Y', strtotime($date_from)) . ' to ' . date('M d, Y', strtotime($date_to));
        $subtitle = strtoupper($panel . 'ly Report');
        return response()->json(
            [

                'categories'    => $categories,
                'series'      => $series,
                'title' => 'Number of Livestock Feeds Available at ' . $company->name . $extra_title,
                'subtitle' => $subtitle,

            ],
            200
        );
    }
    public function medicinesInStock(Request $request)
    {
        $company_id = $this->getUserCompany();
        $company = Company::find($company_id);
        $date_from = Carbon::now()->startOfMonth();
        $date_to = Carbon::now()->endOfMonth();
        $panel = 'month';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        $categories = [];
        $quantity = [];
        $consumed = [];
        $balance = [];

        $livestock_medicines = LiveStockMedicine::where('company_id', $company_id)->get();
        foreach ($livestock_medicines as $livestock_medicine) {
            $medicine_in_stock = MedicineStockBatch::groupBy('medicine_id')->where(['company_id' => $company_id, 'medicine_id' => $livestock_medicine->id])->select('*', \DB::raw('SUM(quantity_stocked) * net_weight as quantity_stocked'), \DB::raw('SUM(consumed) * net_weight as consumed'), \DB::raw('SUM(balance) * net_weight as balance'))->where('company_id', $company_id)/*->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)*/->where('confirmed_by', '!=', null)->first();


            $unit_of_measurement = '';
            $total_stocked = 0;
            $quantity_consumed = 0;
            $total_balance = 0;
            if ($medicine_in_stock) {
                $unit_of_measurement = ' (' . $medicine_in_stock->unit_of_measurement . ')';
                $total_stocked = $medicine_in_stock->quantity_stocked;
                $quantity_consumed = $medicine_in_stock->consumed;
                $total_balance = $medicine_in_stock->balance;
            }
            $medicine_name = $livestock_medicine->name . $unit_of_measurement;
            $categories[] = $medicine_name;
            $quantity[] = [
                'name' => $medicine_name,
                'y' => (int) $total_stocked,
            ];
            $consumed[] = [
                'name' => $medicine_name,
                'y' => (int) $quantity_consumed,
            ];

            $balance[] = [
                'name' => $medicine_name,
                'y' => (int) $total_balance,
            ];
        }
        $series = [
            [
                'name' => 'Total Stock',
                'data' => $quantity, //array format
                // 'color' => '#333333',
                //'stack' => 'Initial Stock'
            ],
            [
                'name' => 'Consumed',
                'data' => $consumed, //array format
                'color' => '#f39c12',
                //'stack' => 'Sold'
            ],
            [
                'name' => 'Balance',
                'data' => $balance, //array format
                'color' => '#00a65a',
                //'stack' => 'Available'
            ],
            // [
            //     'name' => 'Dead',
            //     'data' => $dead, //array format
            //     'color' => '#DC143C',
            //     //'stack' => 'Dead'
            // ],
        ];
        $extra_title = ' from ' . date('M d, Y', strtotime($date_from)) . ' to ' . date('M d, Y', strtotime($date_to));
        $subtitle = strtoupper($panel . 'ly Report');
        return response()->json(
            [

                'categories'    => $categories,
                'series'      => $series,
                'title' => 'Number of Livestock Medicines Available at ' . $company->name . $extra_title,
                'subtitle' => $subtitle,

            ],
            200
        );
    }
    public function auditTrails(Request $request)
    {
        //     $schedule = new Schedule();
        //     $list = $schedule
        //   ->command('backup:run')
        //   ->onFailure(function () {
        //         return 'failed';
        //   })
        //   ->onSuccess(function () {
        //         return 'success';
        //   });
        $user_id = $this->getCurrentUser()->id;
        $date_from = Carbon::now()->startOfWeek();
        $date_to = Carbon::now()->endOfWeek();
        $panel = 'week';
        if (isset($request->from, $request->to, $request->panel)) {
            $date_from = date('Y-m-d', strtotime($request->from)) . ' 00:00:00';
            $date_to = date('Y-m-d', strtotime($request->to)) . ' 23:59:59';
            $panel = $request->panel;
        }
        $activity_logs = Notification::where('notifiable_id', $user_id)->where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->orderBy('created_at', 'DESC')->paginate(20);
        return response()->json(compact('activity_logs'), 200);
    }
    public function markAsRead()
    {
        $user = $this->getCurrentUser();
        $user->unreadNotifications->markAsRead();
        return response()->json([], 204);
    }
    public function backUps()
    {
        $date = Date('Y-m-d', strtotime('now'));
        $file_name = 'db_backup_' . $date . '.sql';
        $url = 'storage/bkup/db/' . $file_name;
        // $directories = Storage::allDirectories($directory);

        return response()->json(compact('url', 'date'), 200);
    }
}
