/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const reportsRoutes = {
  path: '/reports',
  component: Layout,
  redirect: '/reports/feed-cost',
  name: 'Reports',
  alwaysShow: true,
  meta: {
    title: 'Reports',
    icon: 'el-icon-document',
  },
  children: [
    {
      component: () => import ('./views/Eggs'),
      path: '/reports/eggs',
      name: 'EggSales',
      meta: {
        title: 'Egg Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
    {
      component: () => import ('./views/Livestock'),
      path: '/reports/livestock',
      name: 'LiveStockSales',
      meta: {
        title: 'Livestock Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
    {
      component: () => import ('./views/Fertilizers'),
      path: '/reports/fertilizer',
      name: 'FertilizerSales',
      meta: {
        title: 'Fertilizer Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
  ],
};

export default reportsRoutes;
