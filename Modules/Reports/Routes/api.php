<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('reports')->group(function () {
        Route::get('notification/mark-as-read', 'ReportsController@markAsRead');
        Route::get('income-and-expenses', 'ReportsController@incomeAndExpenses');
        Route::get('livestocks-in-stock', 'ReportsController@livestocksInStock');
        Route::get('feeds-in-stock', 'ReportsController@feedsInStock');
        Route::get('medicines-in-stock', 'ReportsController@medicinesInStock');
        Route::get('audit-trails', 'ReportsController@auditTrails');/*->middleware('permission:view reports');*/
    });
});
