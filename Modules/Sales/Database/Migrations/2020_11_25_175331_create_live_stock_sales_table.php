<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveStockSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_stock_sales', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_batches_id');
            $table->integer('livestock_id');
            $table->double('amount',10,2);
            $table->integer('quantity');
            $table->integer('sold_by');
            $table->integer('confirmed_by');
            $table->double('rate',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_stock_sales');
    }
}
