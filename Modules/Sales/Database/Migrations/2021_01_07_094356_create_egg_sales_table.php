<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEggSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('egg_sales', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('livestock_id');
            $table->integer('egg_stock_id');
            $table->integer('quantity');
            $table->double('rate', 10, 2);
            $table->double('total_amount', 10, 2);
            $table->integer('sold_by');
            $table->integer('confirmed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('egg_sales');
    }
}
