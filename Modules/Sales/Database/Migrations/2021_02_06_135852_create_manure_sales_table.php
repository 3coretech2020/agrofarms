<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManureSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manure_sales', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('quantity');
            $table->double('rate', 10, 2);
            $table->double('total_amount', 10, 2);
            $table->integer('sold_by');
            $table->integer('confirmed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manure_sales');
    }
}
