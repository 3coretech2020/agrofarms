<?php

namespace Modules\Sales\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\EggStock;
use Modules\Livestock\Entities\LiveStock;

class EggSale extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function eggStock()
    {
        return $this->belongsTo(EggStock::class, 'egg_stock_id', 'id');
    }
    public function seller()
    {
        return $this->belongsTo(User::class, 'sold_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
