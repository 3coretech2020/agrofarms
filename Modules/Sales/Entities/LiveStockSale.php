<?php

namespace Modules\Sales\Entities;

use App\User;
use App\Company;
use Illuminate\Database\Eloquent\Model;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;

class LiveStockSale extends Model
{
    protected $fillable = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function batch()
    {
        return $this->belongsTo(LiveStockBatch::class, 'live_stock_batch_id', 'id');
    }
    public function liveStock()
    {
        return $this->belongsTo(LiveStock::class, 'livestock_id', 'id');
    }
    public function seller()
    {
        return $this->belongsTo(User::class, 'sold_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
