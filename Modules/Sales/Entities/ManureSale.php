<?php

namespace Modules\Sales\Entities;

use App\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\Manure;

class ManureSale extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function manure()
    {
        return $this->belongsTo(Manure::class);
    }
    public function seller()
    {
        return $this->belongsTo(User::class, 'sold_by', 'id');
    }
    public function confirmer()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }
}
