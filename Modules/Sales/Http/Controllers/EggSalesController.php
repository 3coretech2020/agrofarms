<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\EggStock;
use Modules\Sales\Entities\EggSale;

class EggSalesController extends Controller
{
    public function index()
    {
        $company_id = $this->getUserCompany();
        $egg_sales = EggSale::with(['liveStock.category', 'seller', 'confirmer', 'eggStock'])->where('company_id', $company_id)->orderBy('id', 'DESC')->get();
        // if ($egg_salees->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('egg_sales'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $egg_sale = new EggSale();
        $egg_sale->company_id = $company_id;;
        $egg_sale->livestock_id = $request->livestock_id;
        $egg_sale->quantity_in_crates = $request->quantity_in_crates;
        $egg_sale->quantity = $request->quantity_in_crates * 30; // Convert quantity in crate to pieces
        $egg_sale->total_amount = $request->total_amount;
        $egg_sale->sold_by = $user->id;
        $egg_sale->rate = $egg_sale->total_amount / $egg_sale->quantity;

        if ($egg_sale->save()) {
            // $egg_sale->eggStock->sold += $request->quantity;
            // $egg_sale->eggStock->balance -= $request->quantity;
            // $egg_sale->eggStock->save();

            // log this activity
            $title = 'Sale of Eggs made';
            $description = $egg_sale->quantity . ' ' . $egg_sale->liveStock->name . " eggs were sold by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($egg_sale);
        }
        $message = 'Eggs sales record could not be saved';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(EggSale $egg_sale)
    {
        $egg_sale =  $egg_sale->with(['liveStock.category', 'seller', 'confirmer', 'eggStock'])->find($egg_sale->id);
        return response()->json(compact('egg_sale'), 200);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, EggSale $egg_sale)
    {
        //
        $user = $this->getCurrentUser();
        if ($egg_sale->confirmed_by === null) {
            # code...

            $egg_sale->livestock_id = $request->livestock_id;
            $egg_sale->quantity_in_crates = $request->quantity_in_crates;
            $egg_sale->quantity = $request->quantity_in_crates * 30; // Convert quantity in crate to pieces
            $egg_sale->total_amount = $request->total_amount;
            $egg_sale->rate = $egg_sale->total_amount / $egg_sale->quantity;
            if ($egg_sale->save()) {
                // log this activity
                $title = 'Egg sales details modified';
                $description = "Egg sales details was updated by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($egg_sale);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmSale(Request $request, EggSale $egg_sale)
    {
        //
        $user = $this->getCurrentUser();
        $egg_sale->confirmed_by = $user->id;
        if ($egg_sale->save()) {
            $this->deductEggStock($egg_sale->quantity, $egg_sale->livestock_id);
            // log this activity
            $title = 'Egg sales confirmed';
            $description = $egg_sale->quantity . " eggs sold was confirmed by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
        }
        return $this->show($egg_sale);
    }

    private function deductEggStock($quantity_to_sell, $livestock_id)
    {
        $company_id = $this->getUserCompany();
        $egg_stocks = EggStock::where(['company_id' => $company_id, 'livestock_id' => $livestock_id])->whereRaw('balance > 0')->get();
        foreach ($egg_stocks as $egg_stock) {
            $balance = $egg_stock->balance;
            if ($balance > $quantity_to_sell) {
                $egg_stock->balance -= $quantity_to_sell;
                $egg_stock->sold += $quantity_to_sell;
                $egg_stock->save();
                $quantity_to_sell = 0;
                break;
            } else {
                $egg_stock->balance = 0;
                $egg_stock->sold += $balance;
                $egg_stock->save();
                $quantity_to_sell -= $balance;
            }
        }
        return $quantity_to_sell;
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(EggSale $egg_sale)
    {
        $user = $this->getCurrentUser();

        if ($egg_sale->confirmed_by == null) {
            // log this activity
            $title = 'Egg sales details deleted';
            $description = $egg_sale->quantity . " of unconfirmed eggs sold was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];

            $egg_sale->delete();
            $this->logUserActivity($title, $description, $roles);
            return response()->json([], 204);
        }
        $message = 'Record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
