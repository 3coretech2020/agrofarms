<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Livestock\Entities\Feeding;
use Modules\Livestock\Entities\LiveStock;
use Modules\Livestock\Entities\LiveStockBatch;
use Modules\Livestock\Entities\Medication;
use Modules\Sales\Entities\LiveStockSale;

class LiveStockSalesController extends Controller
{
    public function index()
    {
        $company_id = $this->getUserCompany();
        $livestock_sales = LiveStockSale::with(['liveStock.category', 'seller', 'confirmer', 'batch'])->where('company_id', $company_id)->orderBy('id', 'DESC')->get();
        // if ($livestock_salees->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('livestock_sales'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $livestock_sale = new LiveStockSale();
        $livestock_sale->company_id = $company_id;;
        $livestock_sale->livestock_id = $request->livestock_id;
        $livestock_sale->live_stock_batch_id = $request->live_stock_batch_id;
        $livestock_sale->quantity = $request->quantity; // Convert quantity in crate to pieces
        $livestock_sale->amount = $request->amount;
        $livestock_sale->rate = $request->amount / $request->quantity;

        $livestock_sale->sold_by = $user->id;

        if ($livestock_sale->save()) {
            // $livestock_sale->livestockStock->sold += $request->quantity;
            // $livestock_sale->livestockStock->balance -= $request->quantity;
            // $livestock_sale->livestockStock->save();

            // log this activity
            $title = 'Sale of LiveStocks made';
            $description = $livestock_sale->quantity . ' ' . $livestock_sale->liveStock->name . " were sold by " . $user->name . ' from batch: ' . $livestock_sale->batch->batch_no;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
            return $this->show($livestock_sale);
        }
        $message = 'LiveStocks sales record could not be saved';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(LiveStockSale $livestock_sale)
    {
        $livestock_sale =  $livestock_sale->with(['liveStock.category', 'seller', 'confirmer', 'batch'])->find($livestock_sale->id);
        return response()->json(compact('livestock_sale'), 200);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, LiveStockSale $livestock_sale)
    {
        //
        $user = $this->getCurrentUser();
        if ($livestock_sale->confirmed_by === null) {
            # code...

            $livestock_sale->livestock_id = $request->livestock_id;
            $livestock_sale->live_stock_batch_id = $request->live_stock_batch_id;
            $livestock_sale->quantity = $request->quantity; // Convert quantity in crate to pieces
            $livestock_sale->amount = $request->amount;
            $livestock_sale->rate = $request->amount / $request->quantity;
            if ($livestock_sale->save()) {
                // log this activity
                $title = 'Livestock sales details modified';
                $description = "Livestock sales details was updated by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($livestock_sale);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmSale(Request $request, LiveStockSale $livestock_sale)
    {
        //
        $user = $this->getCurrentUser();
        $livestock_sale->confirmed_by = $user->id;
        if ($livestock_sale->save()) {
            $this->sellOutLivestockBatch($livestock_sale);
            // log this activity
            $title = 'LiveStock sales confirmed';
            $description = $livestock_sale->quantity . ' ' . $livestock_sale->liveStock->name . " sold was confirmed by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
        }
        return $this->show($livestock_sale);
    }

    private function sellOutLivestockBatch(LiveStockSale $livestock_sale)
    {
        $livestock_sale->batch->quantity_sold += $livestock_sale->quantity;
        $livestock_sale->batch->save();
    }
    public function costOfRearing($livestock_batch_id)
    {
        $livestock_obj = new LiveStock();
        $company_id = $this->getUserCompany();
        $cost_of_rearing_each = $livestock_obj->costOfRearing($company_id, $livestock_batch_id);
        return response()->json(compact('cost_of_rearing_each'), 200);
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(LiveStockSale $livestock_sale)
    {
        if ($livestock_sale->confirmed_by === null) {
            $user = $this->getCurrentUser();

            // log this activity
            $title = 'LiveStock sales details deleted';
            $description = $livestock_sale->quantity . " of livestocks sold was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];

            $livestock_sale->delete();
            $this->logUserActivity($title, $description, $roles);
            return response()->json([], 204);
        }
        $message = 'Record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
