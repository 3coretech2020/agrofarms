<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Inventory\Entities\Manure;
use Modules\Sales\Entities\ManureSale;

class ManureSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company_id = $this->getUserCompany();
        $fertilizer_sales = ManureSale::with(['seller', 'confirmer'])->where('company_id', $company_id)->orderBy('id', 'DESC')->get();
        // if ($manure_salees->count() < 1) {
        //     $message = 'Not Found';
        //     return response()->json(compact('message'), 404);
        // }
        return response()->json(compact('fertilizer_sales'), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = $this->getCurrentUser(); // this is the user that is performing the task of stocking
        $company_id = $this->getUserCompany();
        $type = $request->type;
        $manure = Manure::where(['company_id' => $company_id, 'type' => $type])->first();
        if ($manure) {
            $manure_sale = new ManureSale();

            $manure_sale->company_id = $company_id;
            $manure_sale->manure_id = $manure->id;
            $manure_sale->quantity = $request->quantity; // Convert quantity in crate to pieces
            $manure_sale->total_amount = $request->total_amount;
            $manure_sale->sold_by = $user->id;
            $manure_sale->rate = $manure_sale->total_amount / $manure_sale->quantity;

            if ($manure_sale->save()) {
                // $manure_sale->manureStock->sold += $request->quantity;
                // $manure_sale->manureStock->balance -= $request->quantity;
                // $manure_sale->manureStock->save();

                // log this activity
                $title = 'Sale of ' . $type . ' made';
                $description = $manure_sale->quantity . ' bags of ' . $type . ' were sold by ' . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);
                return $this->show($manure_sale);
            }
            $message = 'Fertilizers sales record could not be saved';
            return response()->json(compact('message'), 500);
        }
        $message = $type . ' record was not found';
        return response()->json(compact('message'), 500);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(ManureSale $manure_sale)
    {
        $fertilizer_sale =  $manure_sale->with(['seller', 'confirmer'])->find($manure_sale->id);
        return response()->json(compact('fertilizer_sale'), 200);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, ManureSale $manure_sale)
    {
        //
        $user = $this->getCurrentUser();
        if ($manure_sale->confirmed_by === null) {
            # code...

            $manure_sale->quantity = $request->quantity; // Convert quantity in crate to pieces
            $manure_sale->total_amount = $request->total_amount;
            $manure_sale->rate = $manure_sale->total_amount / $manure_sale->quantity;
            if ($manure_sale->save()) {
                // log this activity
                $title = $manure_sale->manure->type . ' sales details modified';
                $description = "Manure sales details was updated by " . $user->name;
                $roles = ['auditor', 'farm-officer'];
                $this->logUserActivity($title, $description, $roles);

                return $this->show($manure_sale);
            }
        }
        $message = 'Changes could not be effected';
        return response()->json(compact('message'), 500);
    }
    public function confirmSale(Request $request, ManureSale $manure_sale)
    {
        //
        $user = $this->getCurrentUser();
        $manure_sale->confirmed_by = $user->id;
        if ($manure_sale->save()) {
            $this->deductEggStock($manure_sale->manure_id, $manure_sale->quantity);
            // log this activity
            $title = $manure_sale->manure->type . ' sales confirmed';
            $description = $manure_sale->quantity . " bags of " . $manure_sale->manure->type . " sold was confirmed by " . $user->name;
            $roles = ['auditor', 'farm-officer'];
            $this->logUserActivity($title, $description, $roles);
        }
        return $this->show($manure_sale);
    }

    private function deductEggStock($manure_id, $quantity_to_sell)
    {
        $company_id = $this->getUserCompany();
        $manure_stock = Manure::find($manure_id)->first();
        $manure_stock->balance -= $quantity_to_sell;
        $manure_stock->sold += $quantity_to_sell;
        $manure_stock->save();
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(ManureSale $manure_sale)
    {

        if ($manure_sale->confirmed_by == null) {
            $user = $this->getCurrentUser();
            // log this activity
            $title = $manure_sale->manure->type . ' sales details deleted';
            $description = $manure_sale->quantity . " unconfirmed bags of " . $manure_sale->manure->type . " sold was deleted by " . $user->name;
            $roles = ['auditor', 'farm-officer'];

            $manure_sale->delete();
            $this->logUserActivity($title, $description, $roles);
            return response()->json([], 204);
        }
        $message = 'Record could not be deleted';
        return response()->json(compact('message'), 500);
    }
}
