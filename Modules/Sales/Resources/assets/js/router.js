/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const salesRoutes = {
  path: '/sales',
  component: Layout,
  redirect: '/sales/feed-cost',
  name: 'Sales',
  alwaysShow: true,
  meta: {
    title: 'Sales',
    icon: 'el-icon-sold-out',
  },
  children: [
    {
      component: () => import ('./views/Eggs'),
      path: '/sales/eggs',
      name: 'EggSales',
      meta: {
        title: 'Egg Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
    {
      component: () => import ('./views/Livestock'),
      path: '/sales/livestock',
      name: 'LiveStockSales',
      meta: {
        title: 'Livestock Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
    {
      component: () => import ('./views/Fertilizers'),
      path: '/sales/fertilizer',
      name: 'FertilizerSales',
      meta: {
        title: 'Fertilizer Sales',
        // roles: ['admin', 'auditor'],
        // permissions: ['manage expenses'],
      },
    },
  ],
};

export default salesRoutes;
