<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {


    Route::prefix('sales')->group(function () {
        //////////////livestockfeed Routes/////////////////
        Route::prefix('eggs')->group(function () {
            Route::get('/', 'EggSalesController@index');
            Route::post('/store', 'EggSalesController@store');
            Route::put('/update/{egg_sale}', 'EggSalesController@update');
            Route::delete('/destroy/{egg_sale}', 'EggSalesController@destroy');
            Route::put('/confirm-sales/{egg_sale}', 'EggSalesController@confirmSale');
        });
        ///////////livestockmedicine Routes//////////////////
        Route::prefix('livestocks')->group(function () {
            Route::get('/', 'LiveStockSalesController@index');
            Route::post('/store', 'LiveStockSalesController@store');
            Route::put('/update/{livestock_sale}', 'LiveStockSalesController@update');
            Route::delete('/destroy/{livestock_sale}', 'LiveStockSalesController@destroy');
            Route::put('/confirm-sales/{livestock_sale}', 'LiveStockSalesController@confirmSale');
            Route::get('/cost-of-rearing/{livestock_batch_id}', 'LiveStockSalesController@costOfRearing');
        });
        Route::prefix('fertilizer')->group(function () {
            Route::get('/', 'ManureSalesController@index');
            Route::post('/store', 'ManureSalesController@store');
            Route::put('/update/{manure_sale}', 'ManureSalesController@update');
            Route::delete('/destroy/{manure_sale}', 'ManureSalesController@destroy');
            Route::put('/confirm-sales/{manure_sale}', 'ManureSalesController@confirmSale');
        });
    });
});
