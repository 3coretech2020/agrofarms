<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Livestock\Entities\LiveStock;

class Company extends Model
{
    //

    public function staff()
    {
        return $this->hasMany(Staff::class, 'company_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function livestocks()
    {
        return $this->belongsToMany(LiveStock::class);
    }
}
