<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'username' => 'required|string|unique:users',
            'phone_number' => 'required',
            'password' => 'required|string|',
            'c_password' => 'required|same:password',
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone_number' => $request->phone_number,
            'password' => bcrypt($request->password),
            'api_token' => Str::random(60),
        ]);
        if ($user->save()) {
            // $user->
            return response()->json([
                'message' => 'Successfully created user!'
            ], 201);
        } else {
            return response()->json(['error' => 'Provide proper details']);
        }
    }
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Invalid Credentials'
            ], 401);
        }


        $user = $request->user();
        $user_resource = new UserResource($user);
        $tokenResult = $user->createToken('Personal Access Token');
        // $token = $tokenResult->token;
        // // $token->expires_at = Carbon::now()->addHour(3);
        // // if ($request->remember_me)
        // //     $token->expires_at = Carbon::now()->addWeeks(1);
        // $token->save();
        // return response()->json($user_resource, Response::HTTP_OK)->header('Authorization', $tokenResult->plainTextToken);
        return response()->json([
            'user_data' => $user_resource
        ])->header('Authorization', $tokenResult->plainTextToken);
        // ->header('Expiry', strtotime($tokenResult->token->expires_at));
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        // $request->user()->token()->revoke();
        // return response()->json([
        //     'message' => 'Successfully logged out'
        // ]);
        $user = $request->user();
        // $title = "Log out action";
        // $description = $user->name . ' logged out of the portal';
        // $this->logUserActivity($title, $description);
        $user->tokens()->delete();
        return response()->json([], Response::HTTP_OK);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user()
    {
        return new UserResource(Auth::user());
    }
}
