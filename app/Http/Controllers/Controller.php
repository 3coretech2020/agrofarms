<?php

namespace App\Http\Controllers;

use App\Company;
use App\Notifications\AuditTrail;
use App\UnsyncTable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Notification;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\User;
use Modules\Livestock\Entities\LiveStock;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $user;
    public function setUser()
    {
        $this->user  = Auth::user();
    }

    public function getCurrentUser()
    {
        $this->setUser();

        return $this->user;
    }
    public function getUserCompany()
    {
        return $this->getCurrentUser()->staff->company_id;
    }
    public function getCompanyLivestocks($company_id)
    {
        $company = Company::find($company_id);
        return $company->livestocks()->with('category')->orderBy('name')->get(); //LiveStock::with('category')->orderBy('name')->get();
    }
    public function getBatchNo($prefix, $next_no)
    {
        $no_of_digits = 5;

        $digit_of_next_no = strlen($next_no);
        $unused_digit = $no_of_digits - $digit_of_next_no;
        $zeros = '';
        for ($i = 1; $i <= $unused_digit; $i++) {
            $zeros .= '0';
        }

        return $prefix . $zeros . $next_no;
    }

    public function logUserActivity($title, $description, $roles = [])
    {
        // $user = $this->getUser();
        // if ($role) {
        //     $role->notify(new AuditTrail($title, $description));
        // }
        // return $user->notify(new AuditTrail($title, $description));
        // send notification to admin at all times
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', '=', 'admin'); // this is the role id inside of this callback
        })->get();

        if (in_array('farm-officer', $roles)) {
            $farm_officer = User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'farm-officer'); // this is the role id inside of this callback
            })->get();
            $users = $users->merge($farm_officer);
        }
        if (in_array('auditor', $roles)) {
            $auditor = User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'auditor'); // this is the role id inside of this callback
            })->get();
            $users = $users->merge($auditor);
        }
        // var_dump($users);
        $notification = new AuditTrail($title, $description);
        return Notification::send($users->unique(), $notification);
        // $activity_log = new ActivityLog();
        // $activity_log->user_id = $user->id;
        // $activity_log->action = $action;
        // $activity_log->user_type = $user->roles[0]->name;
        // $activity_log->save();
    }
    public function userNotifications()
    {
        $user = $this->getCurrentUser();
        $notifications = $user->unreadNotifications()->orderBy('created_at', 'DESC')->get();
        return response()->json(compact('notifications'), 200);
    }

    public function fillUnsyncTable($table_name, $table_id)
    {
        // $unsync_table = new UnsyncTable();
        // $unsync_table->create([
        //     'table_name' => $table_name,
        //     'table_id' => $table_id,
        //     'is_synced' => 0
        // ]);
    }
}
