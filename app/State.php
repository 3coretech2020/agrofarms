<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    public function staff()
    {
        return $this->hasMany(Staff::class, 'state_id', 'id');
    }

    public function companies()
    {
        return $this->hasMany(Company::class, 'state_id', 'id');
    }
}
