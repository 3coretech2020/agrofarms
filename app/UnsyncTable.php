<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnsyncTable extends Model
{
    //
    protected $fillable = ['table_name', 'table_id', 'is_synced'];
}
