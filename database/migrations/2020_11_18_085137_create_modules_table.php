<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('slug');
        });

        Schema::create('activated_modules', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('module_id');
            $table->integer('company_id');
        });

        Schema::create('module_prices', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('module_id');
            $table->double('amount', 10, 2);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
        Schema::dropIfExists('activated_modules');
        Schema::dropIfExists('module_prices');
    }
}
