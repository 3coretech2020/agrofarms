<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('user-notifications', 'Controller@userNotifications');

    Route::get('users', 'UserController@index')->middleware('permission:manage user');
    Route::post('users', 'UserController@store')->middleware('permission:manage user');
    Route::get('users/{user}', 'UserController@show')->middleware('permission:manage user');
    Route::put('users/{user}', 'UserController@update')->middleware('permission:manage user');

    Route::put('users/update-password/{user}', 'UserController@updatePassword');
    Route::put('users/reset-password/{user}', 'UserController@adminResetUserPassword')->middleware('permission:manage user');

    Route::delete('users/{user}', 'UserController@destroy')->middleware('permission:manage user');
});

// Route::post('company/store', 'CompaniesController@store');
