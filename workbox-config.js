module.exports = {
  globDirectory: 'public/',
  globPatterns: [
    '**/*.{mp3,css,ttf,woff,json,eot,svg,woff2,otf,txt,less,scss,js,ico,xml,png,gif,jpeg,jpg,php,config}',
  ],
  ignoreURLParametersMatching: [
    /^utm_/,
    /^fbclid$/,
  ],
  swDest: 'public/sw.js',
};
